﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Danh mục khu vực</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />    
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: "Kiểm tra thông tin nhập!",
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };

        var successHandler = function (form, action) {
            dsNhanvien.reload();
            nvForm.reset();
            wdNv.hide();
            id.setValue('');
            btnSua.disable();
            btnXoa.disable();
        };

        var sua = function () {
            var s = GridPanel1.getSelectionModel().getSelected().get('Username');
            id.setValue(s);
            var record = GridPanel1.getSelectionModel().getSelected();
            nvForm.getForm().loadRecord(record);
            Pass1.setDisabled(true);
            wdNv.show();
        };

        var changePass = function () {

            if (chkChangePass.getValue() == true) {
                Pass1.setDisabled(false);
                checkChangepass.setValue('true');
            }
            else {
                Pass1.setDisabled(true);
                checkChangepass.setValue('false');
            }
        };
        var phanquyenHandler = function () {
            winPhanQuyen.show();
            GridPanel2.store.reload();
        };
        var selectedRow = function (record) {
            var recordindex = GridPanel2.store.indexOf(record);
            if (record.get('Check') == true)
                GridPanel2.getSelectionModel().selectRow(recordindex, true, false);

        };
</script>
<script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.Store1.DataSource = new object[]
            //{
            //    new object[] {"admin"},
            //    new object[] {"user"}                                    
            //};
            //this.Store1.DataBind();
            
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
        }
</script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Store ID="strDMNSD" runat="server" >
            <Proxy>
                 <ext:HttpProxy  Url="/Hethong/dsNhanvien" />
            </Proxy>
            <Reader>
                <ext:JsonReader IDProperty="MA_NV" Root="data" TotalProperty="total">
                    <Fields>
                        <ext:RecordField Name="MA_NV" />
                        <ext:RecordField Name="TEN_NV" />                                                                                       
                    </Fields>
                </ext:JsonReader>
             </Reader> 
             <BaseParams>
                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
             </BaseParams>                                      
        </ext:Store>
        <ext:Viewport ID="Panel1" runat="server" Layout="FitLayout" Border="false">            
            <Items>                               
                <ext:GridPanel 
                ID="GridPanel1" 
                runat="server" Border="false"              
                TrackMouseOver="true" 
                AutoExpandColumn="GHICHU">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">                                                   
                                <Listeners>
                                    <Click Handler="#{nvForm}.reset();#{chkChangePass}.setValue(true);#{wdNv}.show();" />
                                </Listeners>                                                                             
                            </ext:Button>                                                
                            <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>" Icon="BookEdit">
                                <Listeners>
                                    <Click Fn="sua" />
                                </Listeners>                     
                            </ext:Button>                                                
                            <ext:Button ID="btnXoa" Disabled="true" runat="server" Text="<%$Resources:lang, Delete %>" Icon="Delete">
                                 <DirectEvents>
                                     <Click 
                                        Url="/Hethong/xoaUser" 
                                        CleanRequest="true"
                                        Method="POST" Success="#{dsNhanvien}.reload();">
                                        <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                        <ExtraParams>
                                            <ext:Parameter Name="id" Value="#{GridPanel1}.getSelectionModel().getSelected().get('Username')" Mode="Raw" />
                                        </ExtraParams>
                                     </Click>
                                  </DirectEvents>
                              </ext:Button>   
                              <ext:ToolbarSeparator ID="ToolbarSeparator3" runat="server" />
                            <%--<ext:Button ID="btnPhanQuyen" runat="server" Text="Phân quyền" Icon="UserComment">
                                <Listeners>
                                  <Click Fn="phanquyenHandler" />
                                </Listeners>
                             </ext:Button>   --%>
                             <ext:ToolbarSeparator ID="ToolbarSeparator4" runat="server" />                     
                        </Items>
                    </ext:Toolbar>
                </TopBar>                                                
                <Store>
                    <ext:Store 
                        ID="dsNhanvien" 
                        runat="server" 
                        RemoteSort="true" 
                        UseIdConfirmation="true">
                        <Proxy>
                            <ext:HttpProxy Url="/Hethong/dsUser/" />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="Username" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="Username" />
                                    <ext:RecordField Name="MA_NV" />
                                    <ext:RecordField Name="TEN_NV" />
                                    <ext:RecordField Name="Language" />
                                    <ext:RecordField Name="Theme" />
                                    <ext:RecordField Name="Pass" />
                                    <ext:RecordField Name="ROLE" />
                                    <ext:RecordField Name="GHICHU" />                                                                                                                                  
                                </Fields>
                            </ext:JsonReader>                            
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                        </BaseParams> 
                        <SortInfo Field="Username" Direction="ASC" />                                           
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="Username" DataIndex="Username" Header="Tên truy cập" />                         
                        <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Tên nhân viên" Width="130" /> 
                        <ext:Column ColumnID="Language" DataIndex="Language" Header="Ngôn ngữ" Align="Center" /> 
                        <ext:Column ColumnID="Theme" DataIndex="Theme" Header="Giao diện" Align="Center"/>     
                        <ext:Column ColumnID="ROLE" DataIndex="ROLE" Header="Quyền" Align="Center"/>  
                        <ext:Column ColumnID="GHICHU" DataIndex="GHICHU" Header="Ghi chú" />                                     
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" >
                        <Listeners>                             
                             <RowSelect Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters>                            
                            <ext:StringFilter DataIndex="Username" />
                            <ext:StringFilter DataIndex="TEN_NV" />
                            <ext:StringFilter DataIndex="Language" />       
                            <ext:StringFilter DataIndex="Theme" />  
                            <ext:StringFilter DataIndex="ROLE" />       
                            <ext:StringFilter DataIndex="GHICHU" />                            
                        </Filters>
                    </ext:GridFilters>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="30" />
                </BottomBar>                              
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>                               
                    <RowClick Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                </Listeners>              
                </ext:GridPanel>
                <ext:Window ID="wdNv" runat="server" Hidden="true" Title="Thông tin nhân viên" 
                Icon="Information" Width="530" Height="300" Resizable="false">
                    <Items>
                        <ext:FormPanel ID="nvForm" runat="server" Height="270" Width="520"
                        Border="false" Url="/Hethong/saveUser" ButtonAlign="Center">                
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        <ext:Hidden ID="id" runat="server" />
                                        <ext:Hidden ID="txtPath" runat="server" DataIndex="Path" />
                                        <ext:Hidden ID="txtID" runat="server" DataIndex="ID" />
                                        <ext:Hidden ID="txtMode" runat="server" DataIndex="Mode" />
                                        <ext:Hidden ID="checkChangepass" runat="server" />
                
                                        <ext:TextField ID="Username" DataIndex="Username" runat="server"
                                         FieldLabel="Tên truy cập" AnchorHorizontal="90%" AllowBlank="false"  />
                                        <ext:Checkbox ID="chkChangePass" runat="server" FieldLabel="Đối mật khẩu" >
                                            <Listeners>
                                                <Check Fn="changePass" />                        
                                            </Listeners>
                                        </ext:Checkbox>
                                        <ext:TextField ID="Pass1" runat="server"  
                                        FieldLabel="Mật khẩu" InputType="Password" AnchorHorizontal="90%" />
                                        <ext:ComboBox 
                                        ID="NHANVIEN" 
                                        runat="server" 
                                            AllowBlank="false" 
                                            FieldLabel="Nhân Viên" 
                                            Mode="Local" AnchorHorizontal="70%"
                                            EmptyText="Chọn Nhân Viên" 
                                            StoreID="strDMNSD" ListWidth="300" 
                                            MinChars="1" TriggerAction="All"
                                            DataIndex="MA_NV"
                                            PageSize="10" 
                                            DisplayField="TEN_NV" ValueField="MA_NV" 
                                            TypeAhead="true" ForceSelection="true" ItemSelector="tr.list-item"   >                                
                                        <Template ID="Template1" runat="server">
                                          <Html>
					                        <tpl for=".">
						                        <tpl if="[xindex] == 1">
							                        <table class="cbStates-list">
								                        <tr>
									                        <th style="font-weight:bold; font-family:.VnCourier; font-size:10px;">Mã Nhân Viên</th>
									                        <th style="font-weight:bold; font-family:.VnCourier; font-size:10px;">Tên Nhân Viên</th>
								                        </tr>
						                        </tpl>
						                    <tr class="list-item">
							                    <td style="padding:3px 0px;">{MA_NV}</td>
							                    <td>{TEN_NV}</td>
						                    </tr>
						                  <tpl if="[xcount-xindex]==0">
							                        </table>
						                    </tpl>
					                        </tpl>
				                          </Html>
                                         </Template>
                                         <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                         </Triggers>
                                        <Listeners>                                   
                                                <Select Handler="this.triggers[0].show();" />
                                                <BeforeQuery Handler="this.triggers[0][ this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                <TriggerClick Handler="if (index == 0) { this.clearValue(); this.triggers[0].hide(); }" />
                                    
                                        </Listeners>
                                    </ext:ComboBox>
                                        <ext:ComboBox 
                                                        ID="MA_NHOM" 
                                                        runat="server"
                                                        DataIndex="MA_NHOM"
                                                        TriggerAction="All"                                                        
                                                        Mode="Local" FieldLabel="Nhóm User"                                                    
                                                        AnchorHorizontal="70%"
                                                        DisplayField="TEN_NHOM"
                                                        ValueField="MA_NHOM">
                                                        <Store>
                                                            <ext:Store 
                                                                ID="stNhom" 
                                                                runat="server" 
                                                                RemoteSort="true" 
                                                                UseIdConfirmation="true" >
                                                                <Proxy>
                                                                    <ext:HttpProxy Url="/Hethong/dsNhomUser/" />
                                                                </Proxy>                                                            
                                                                <Reader>
                                                                    <ext:JsonReader IDProperty="MA_NHOM" Root="data" TotalProperty="total">
                                                                        <Fields>
                                                                            <ext:RecordField Name="MA_NHOM" SortDir="ASC"/>  
                                                                            <ext:RecordField Name="TEN_NHOM"/>                                                                                                        
                                                                        </Fields>
                                                                    </ext:JsonReader>
                                                                </Reader>
                                                                <BaseParams>
                                                                    <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                                                    <ext:Parameter Name="start" Value="0" Mode="Raw" />                                                                   
                                                                </BaseParams>                                                                                                                        
                                                            </ext:Store>
                                                        </Store>                                                                                                   
                                                      </ext:ComboBox>         
                                        <ext:TextArea ID="GHICHU" DataIndex="GHICHU" runat="server" 
                                        FieldLabel="Ghi chú" AnchorHorizontal="90%"  />                                                   
                           </Items>
                    </ext:FieldSet>                    
                </Items>
                <BottomBar>
                    <ext:Toolbar ID="Toolbar2" runat="server">  
                        <Items>     
                            <ext:ToolbarFill ID="ToolbarFill1" runat="server" />                                                                         
                            <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <Listeners>
                                    <Click Handler="#{nvForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler });"/>
                                </Listeners>
                            </ext:Button>                                 
                            <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                <Listeners>
                                    <Click Handler="#{nvForm}.reset();" />
                                </Listeners>
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                        </Items>                 
                    </ext:Toolbar>
                </BottomBar>                                
            </ext:FormPanel>
                    </Items>
                    <Listeners>
                        <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
                    </Listeners>
                </ext:Window>  
                
      <ext:Window ID="winPhanQuyen" Modal="true" Hidden="true" runat="server" Title="<center>Phân quyền người dùng</center>" Height="440" Width="600" >
        <Items>           
            <ext:TabPanel  ID="tabPanel1" runat="server" AnchorHorizontal="100%"   AnchorVertical="100%" >
                    <Items>
                        <ext:Panel runat="server" ID="panelFunction" Height="380"  AnchorHorizontal="100%" Title="Phân quyền chức năng" Layout="FitLayout">
                           <Items>
                                <ext:GridPanel  
                                 ID="GridPanel2" 
                                 runat="server" 
                                 Header="false"
                                 Border="false"
                                 Selectable="true"
                                 TrackMouseOver="true"  AnchorHorizontal="100%" AutoWidth="true"  >                                 
                                <Store>
                                  <ext:Store ID="Store2" runat="server" GroupField="TEN_DANHMUC_VN" RefreshAfterSaving="Always" WarningOnDirty="false">
                                    <Proxy>
                                        <ext:HttpProxy Url="/Hethong/dsQuyen/" />
                                    </Proxy>
                                    <Reader>
                                                <ext:JsonReader IDProperty="MA_DMC" Root="data" TotalProperty="total" >
                                                    <Fields>
                                                        <ext:RecordField Name="Check" />
                                                        <ext:RecordField Name="MA_DMC" />
                                                        <ext:RecordField Name="TEN_DMC_VN" />
                                                        <ext:RecordField Name="MA_DANHMUC" />
                                                        <ext:RecordField Name="TEN_DANHMUC_VN" />
                                                        <ext:RecordField Name="NEW" Type="Boolean"  />
                                                        <ext:RecordField Name="EDIT" Type="Boolean" />
                                                        <ext:RecordField Name="DEL" Type="Boolean" />
                                                        <ext:RecordField Name="USER" Type="Boolean" />
                                                        <ext:RecordField Name="LINK_DMC" />
                                                    </Fields>
                                                </ext:JsonReader>
                                    </Reader>
                                    <BaseParams>
                                        <ext:Parameter Name="TenTruyCap" Mode="Raw" Value="#{GridPanel1}.getSelectionModel().getSelected().get('Username')" />
                                    </BaseParams>                                    
                                    </ext:Store>
                                </Store>
                                <ColumnModel ID="ColumnModel2" runat="server">
                                        <Columns>
                                            <ext:CheckColumn ColumnID="Check"  Hidden="true" DataIndex="Check" Header="Check" >                                            
                                            </ext:CheckColumn>
                                            <ext:Column ColumnID="MenuID" Hidden="true" DataIndex="MA_DANHMUC" Header="Danh mục gốc" Width="100" />                                            
                                            <ext:Column ColumnID="MenuName" DataIndex="TEN_DANHMUC_VN" Header="Danh mục" Width="100" />
                                            <ext:Column ColumnID="MenuItemName" DataIndex="TEN_DMC_VN" Header="Chức năng" Width="600" />
                                            <ext:CheckColumn ColumnID="New" DataIndex="NEW" Editable="true" Header="Thêm" />
                                            <ext:CheckColumn ColumnID="Edit" DataIndex="EDIT" Editable="true" Header="Sửa" />
                                            <ext:CheckColumn ColumnID="Del" DataIndex="DEL" Editable="true" Header="Xóa" />
                                            <ext:CheckColumn ColumnID="User" DataIndex="USER" Editable="true" Header="User" />
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel >
                                        <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server"  />                                                               
                                    </SelectionModel>                
                                    <View>
                                       <ext:GroupingView  
                                            ID="GroupingView1"
                                            HideGroupedColumn="true"
                                            ShowGroupName="false"
                                            runat="server" 
                                            ForceFit="true"
                                            StartCollapsed="false"
                                            GroupTextTpl='<span id="ColorCode-{[values.rs[0].data.ColorCode]}"></span>{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
                                            EnableRowBody="true">
                                         <GetRowClass Fn="selectedRow" />
                                        </ext:GroupingView>
                                     </View>
                                    <LoadMask ShowMask="true" />
                                    <SaveMask ShowMask="true" />
                                    <BottomBar>
                                        <ext:Toolbar ID="Toolbar3" runat="server">
                                            <Items>
                                                <ext:ToolbarFill />
                                                <ext:Button ID="btnNhan" Text="Ghi" runat="server" Icon="Disk" >
                                                    <DirectEvents>
                                                        <Click 
                                                        Url="/Hethong/AddUserItems" 
                                                        CleanRequest="true"
                                                        Method="POST"
                                                        Failure="Ext.Msg.show({title:'Error',msg: result.errorMessage,buttons: Ext.Msg.OK,icon: Ext.Msg.ERROR});" 
                                                        Success="#{winPhanQuyen}.hide(); alert('Thành công ^^!');">
                                                        <ExtraParams>                                                            
                                                            <ext:Parameter Name="TenTruyCap" Value="#{GridPanel1}.getSelectionModel().getSelected().get('Username')" Mode="Raw" />
                                                            <ext:Parameter Name="json" Value="Ext.encode(#{GridPanel2}.getRowsValues({selectedOnly:true}))" Mode="Raw" />
                                                        </ExtraParams>
                                                    </Click>
                                                    </DirectEvents>
                                                </ext:Button>                                               
                                            </Items>
                                        </ext:Toolbar>
                                    </BottomBar>
                            </ext:GridPanel>
                           </Items>
                        </ext:Panel>
                    </Items>
                </ext:TabPanel>                
        </Items>
   </ext:Window>              
            </Items>            
        </ext:Viewport>    
    </div>
</body>
</html>
