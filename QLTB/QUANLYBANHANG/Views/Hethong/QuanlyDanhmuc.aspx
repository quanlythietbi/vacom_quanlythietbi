﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Quản lý danh mục</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />    
    <script type="text/javascript">
        var commandHandler = function (cmd, record) {            
            switch (cmd) {
                case "xoa":
                    Ext.Msg.confirm('Thông báo', 'Bạn chắc chắn muốn xóa danh mục ' + record.id + '?', function (btn) {
                        if (btn == "yes") {
                            Ext.Ajax.request({
                                url: '/Hethong/xoaCT',
                                params: { id: record.id },
                                method: 'POST',
                                success: function (response, request) {
                                    dsDanhmuc.reload();
                                },
                                failure: function (result, request) {
                                    alert(response.responseText);
                                }
                            });
                        }
                    });
                    break;
            }
        };
    </script>
<script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
            string themes = Session["themes"].ToString();
            ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
        }
</script>

</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Viewport ID="Panel1" runat="server" Layout="FitLayout" Border="false">            
            <Items>                               
                <ext:GridPanel 
                ID="GridPanel1" 
                runat="server" Border="false"              
                TrackMouseOver="true" AutoExpandColumn="TEN_DMC_VN">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">                                                   
                                <Listeners>
                                    <Click Handler="#{wdDm}.show();" />
                                </Listeners>                                                                             
                            </ext:Button>                                          
                        </Items>
                    </ext:Toolbar>
                </TopBar>                                                
                <Store>
                    <ext:Store 
                        ID="dsDanhmuc" 
                        runat="server"                         
                        UseIdConfirmation="true" GroupField="TEN_DANHMUC_VN">
                        <Proxy>
                            <ext:HttpProxy Url="/Hethong/dsQlDanhmuc/" />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_CTDM" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_CTDM" />
                                    <ext:RecordField Name="MA_DANHMUC" />
                                    <ext:RecordField Name="MA_DMC" />   
                                    <ext:RecordField Name="KIEU_DM_VN"/>    
                                    <ext:RecordField Name="TEN_DANHMUC_VN" />   
                                    <ext:RecordField Name="TEN_DMC_VN" />                                                                                                    
                                </Fields>
                            </ext:JsonReader>
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                        </BaseParams>  
                        <SortInfo Field="KIEU_DM_VN" Direction="ASC" />                                          
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="MA_CTDM" DataIndex="MA_CTDM" Hidden="true" />  
                        <ext:Column ColumnID="TEN_DANHMUC" DataIndex="TEN_DANHMUC_VN" Header="Tên danh mục" Hidden="true" />                    
                        <ext:Column ColumnID="KIEU_DM_VN" DataIndex="KIEU_DM_VN" Header="Kiểu danh mục" Width="150" />             
                        <ext:Column ColumnID="TEN_DMC_VN" DataIndex="TEN_DMC_VN" Header="Tên danh mục con"  />  
                        <ext:CommandColumn Hideable="false" Width="40">
                            <Commands>
                                <ext:GridCommand CommandName="xoa" Icon="Decline">                                                                    
                                      <ToolTip Text="<%$Resources:lang, Delete %>" />
                                </ext:GridCommand>
                            </Commands>                                                      
                       </ext:CommandColumn>                                             
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" >
                        <Listeners>
                             <RowSelect Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters>
                            <ext:DateFilter DataIndex="TEN_DANHMUC" />
                            <ext:StringFilter DataIndex="KIEU_DM_VN" />
                            <ext:StringFilter DataIndex="TEN_DMC" />                            
                        </Filters>
                    </ext:GridFilters>
                </Plugins>
                <View>
                    <ext:GroupingView ID="BufferView1" runat="server" ScrollDelay="0" />
                </View>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="30" />
                </BottomBar>                              
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>
                      <Command Fn="commandHandler" />
                </Listeners>       
                </ext:GridPanel>
                
                <ext:Window ID="wdDm" runat="server" Title="Quản lý cây danh mục" Icon="Information" Layout="FitLayout" Hidden="true" Modal="true" Width="500" Height="500" Plain="true" Resizable="false">
                    <AutoLoad Url="/Hethong/ctQuanlyTree"
                            ReloadOnEvent="true" TriggerEvent="show"
                            ShowMask="true" Mode="IFrame">                   
                    </AutoLoad>   
                </ext:Window>   
            </Items>            
        </ext:Viewport>    
    </div>
</body>
</html>
