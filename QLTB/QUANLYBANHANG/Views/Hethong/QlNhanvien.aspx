﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Danh mục khu vực</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />    
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: "Kiểm tra thông tin nhập!",
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };

        var successHandler = function (form, action) {
            dsNhanvien.reload();
            nvForm.reset();
            wdNv.hide();
            id.setValue('');
            btnSua.disable();
            btnXoa.disable();
        };

        var sua = function () {
            var s = GridPanel1.getSelectionModel().getSelected().get('MA_NV');
            id.setValue(s);
            var record = GridPanel1.getSelectionModel().getSelected();
            nvForm.getForm().loadRecord(record);
            wdNv.show();
        };       
</script>
<script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Store1.DataSource = new object[]
            {
                new object[] {"Điều hành Xe"},
                new object[] {"Vé máy bay"}       
            };
            this.Store1.DataBind();
            this.Store2.DataSource = new object[]
            {
                new object[] {"Phó giám đốc"},
                new object[] {"Quản lý"},
                new object[] {"Trưởng nhóm"},
				new object[] {"Booker"},
				new object[] {"Tài xế"},
				new object[] {"Phụ xe"},
				new object[] {"Làm bến"}           
            };
            this.Store2.DataBind();
            
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
        }
</script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Viewport ID="Panel1" runat="server" Layout="FitLayout" Border="false">            
            <Items>                               
                <ext:GridPanel 
                ID="GridPanel1" 
                runat="server" Border="false"              
                TrackMouseOver="true" 
                AutoExpandColumn="GHICHU">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">                                                   
                                <Listeners>
                                    <Click Handler="#{nvForm}.reset();#{wdNv}.show();" />
                                </Listeners>                                                                             
                            </ext:Button>                                                
                            <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>" Icon="BookEdit">
                                <Listeners>
                                    <Click Fn="sua" />
                                </Listeners>                     
                            </ext:Button>                                                
                            <ext:Button ID="btnXoa" Disabled="true" runat="server" Text="<%$Resources:lang, Delete %>" Icon="Delete">
                                 <DirectEvents>
                                     <Click 
                                        Url="/Hethong/xoaNhanvien" 
                                        CleanRequest="true"
                                        Method="POST" Success="#{dsNhanvien}.reload();">
                                        <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                        <ExtraParams>
                                            <ext:Parameter Name="id" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_NV')" Mode="Raw" />
                                        </ExtraParams>
                                     </Click>
                                  </DirectEvents>
                              </ext:Button>                       
                        </Items>
                    </ext:Toolbar>
                </TopBar>                                                
                <Store>
                    <ext:Store 
                        ID="dsNhanvien" 
                        runat="server" 
                        RemoteSort="true" 
                        UseIdConfirmation="true">
                        <Proxy>
                            <ext:HttpProxy Url="/Hethong/dsNhanvien/" />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_NV" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_NV"/> 
                                    <ext:RecordField Name="TEN_NV" />
                                    <ext:RecordField Name="PHONG" />
                                    <ext:RecordField Name="CHUCVU" />
                                    <ext:RecordField Name="NGAY_SINH" Type="Date" />        
                                    <ext:RecordField Name="GIOI_TINH" />        
                                    <ext:RecordField Name="CMND" />        
                                    <ext:RecordField Name="NGAY_CAP" Type="Date" />        
                                    <ext:RecordField Name="NOI_CAP" />        
                                    <ext:RecordField Name="EMAIL" />   
                                    <ext:RecordField Name="TEN_TAT" />   
                                    <ext:RecordField Name="NICK_CHAT" />        
                                    <ext:RecordField Name="HOKHAU" />        
                                    <ext:RecordField Name="DIACHI" />        
                                    <ext:RecordField Name="TAIKHOAN" />        
                                    <ext:RecordField Name="SDT" />        
                                    <ext:RecordField Name="SDD" />        
                                    <ext:RecordField Name="SDT_FAMILY" />                            
                                    <ext:RecordField Name="GHICHU" />                                                                                                                                         
                                </Fields>
                            </ext:JsonReader>                            
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                        </BaseParams> 
                        <SortInfo Field="PHONG" Direction="ASC" />                                           
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="MA_NV" DataIndex="MA_NV" Header="Mã nhân viên" />                         
                        <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Tên nhân viên" Width="130" /> 
                        <ext:Column ColumnID="PHONG" DataIndex="PHONG" Header="Phòng" Align="Center" /> 
                        <ext:Column ColumnID="CHUCVU" DataIndex="CHUCVU" Header="Chức vụ" Align="Center"/>     
                        <ext:Column ColumnID="SDD" DataIndex="SDD" Header="Điện thoại" Align="Center" Width="80" /> 
						<ext:DateColumn ColumnID="NGAY_SINH" DataIndex="NGAY_SINH" Header="Ngày sinh" Align="Center" Width="80" Format="dd/MM/yyyy"/>
						<ext:Column ColumnID="DIACHI" DataIndex="DIACHI" Header="Địa chỉ" Width="280"/> 	
                        <ext:Column ColumnID="GHICHU" DataIndex="GHICHU" Header="Ghi chú" />                                    
                    </Columns>
                </ColumnModel>                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server">
                        <Listeners>                             
                             <RowSelect Handler="#{btnSua}.enable(); #{btnXoa}.enable();"/>
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters>                            
                            <ext:StringFilter DataIndex="MA_NV" />
                            <ext:StringFilter DataIndex="TEN_NV" />
                            <ext:StringFilter DataIndex="PHONG" />       
                            <ext:StringFilter DataIndex="CHUCVU" />       
                            <ext:StringFilter DataIndex="GHICHU" />                            
                        </Filters>
                    </ext:GridFilters>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="30" />
                </BottomBar>                              
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>                               
                    <RowClick Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                </Listeners>              
                </ext:GridPanel>
                <ext:Window ID="wdNv" runat="server" Hidden="true" Title="Thông tin nhân viên" 
                Icon="Information" Width="530" Height="410" Resizable="false">
                    <Items>
                        <ext:FormPanel ID="nvForm" runat="server" Height="380" Width="520"
                        Border="false" Url="/Hethong/saveNhanvien" ButtonAlign="Center">                
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        <ext:Hidden ID="id" runat="server" />
                                        <ext:TextField 
                                         ID="MA_NV" 
                                         DataIndex="MA_NV"
                                         runat="server" IndicatorText="*"   
                                         AnchorHorizontal="48%" IndicatorCls="red"                            
                                         FieldLabel="Mã nhân viên"
                                         AllowBlank="false"/>

                                         <ext:TextField 
                                         ID="TEN_NV" IndicatorText="*"   
                                         DataIndex="TEN_NV" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="95%"                          
                                         FieldLabel="Tên nhân viên"/>
                                         <ext:TextField 
                                         ID="TEN_TAT"   
                                         DataIndex="TEN_TAT"  
                                         runat="server" AnchorHorizontal="48%"                          
                                         FieldLabel="Tên gọi tắt"/>
                                          <ext:CompositeField ID="CompositeField6" runat="server" FieldLabel="Giới tính" AnchorHorizontal="95%">
                                              <Items>
                                                    <ext:RadioGroup ID="grGioitinh" runat="server" Flex="3" FieldLabel="Giới tính" >
                                                        <Items>
                                                            <ext:Radio ID="rdNam" runat="server" BoxLabel="Nam" Checked="true" InputValue="nam" />
                                                            <ext:Radio ID="rdNu" runat="server" BoxLabel="Nữ" InputValue="nu" />
                                                        </Items>
                                                    </ext:RadioGroup> 
                                                    <ext:DisplayField ID="DisplayField6" runat="server" Margins="0 0 0 10"  Flex="5" />   
                                              </Items>
                                        </ext:CompositeField>
                                                                                   
                                        <ext:CompositeField ID="CompositeField2" runat="server" FieldLabel="Ngày sinh" AnchorHorizontal="95%">
                                              <Items>
                                                    <ext:DateField 
                                                     ID="NGAY_SINH" 
                                                     DataIndex="NGAY_SINH"
                                                     runat="server" 
                                                     Flex="3" />
                                                    <ext:DisplayField ID="DisplayField1" runat="server" Margins="0 0 0 10" Text="SĐT:" Flex="2" />
                                                    <ext:TextField 
                                                     ID="SDT" 
                                                     DataIndex="SDT"
                                                     runat="server" 
                                                     Flex="3" />                                                                                
                                              </Items>
                                        </ext:CompositeField>                                             

                                        <ext:CompositeField ID="CompositeField5" runat="server" FieldLabel="SDD" AnchorHorizontal="95%">
                                              <Items>                                                                                
                                                    <ext:TextField 
                                                     ID="SDD" 
                                                     DataIndex="SDD"
                                                     runat="server" 
                                                     Flex="3" />
                                                     <ext:DisplayField ID="DisplayField5" runat="server" Margins="0 0 0 10" Text="ĐT người thân:" Flex="2" />
                                                     <ext:TextField 
                                                     ID="SDT_FAMILY"
                                                     DataIndex="SDT_FAMILY" 
                                                     runat="server" Flex="3" />
                                              </Items>
                                        </ext:CompositeField>   

                                        <ext:CompositeField ID="CompositeField1" runat="server" FieldLabel="Bộ phận" AnchorHorizontal="95%">
                                              <Items>
                                                    <ext:ComboBox ID="PHONG" AllowBlank="false" Flex="3" DataIndex="PHONG" runat="server"  
                                                      Mode="Local" ValueField="name" DisplayField="name" >
                                                        <Store>
                                                              <ext:Store 
                                                                        ID="Store1" 
                                                                        runat="server" 
                                                                        RemoteSort="true"                                                             
                                                                        UseIdConfirmation="true">                                                            
                                                                        <Reader>
                                                                            <ext:ArrayReader>
                                                                                <Fields>                                                                        
                                                                                    <ext:RecordField Name="name" />
                                                                                </Fields>
                                                                            </ext:ArrayReader>
                                                                        </Reader>                                                                                                       
                                                                        <SortInfo Field="name" Direction="ASC" />
                                                                    </ext:Store>
                                                                </Store>     
                                                    </ext:ComboBox>
                                                    <ext:DisplayField ID="DisplayField4" runat="server" Margins="0 0 0 10" Text="Chức vụ:" Flex="2" />
                                                    <ext:ComboBox ID="CHUCVU" AllowBlank="false" DataIndex="CHUCVU" runat="server"  
                                                     Mode="Local" ValueField="name" DisplayField="name" Flex="3" >
                                                        <Store>
                                                              <ext:Store 
                                                                        ID="Store2" 
                                                                        runat="server" 
                                                                        RemoteSort="true"                                                             
                                                                        UseIdConfirmation="true">                                                            
                                                                        <Reader>
                                                                            <ext:ArrayReader>
                                                                                <Fields>                                                                        
                                                                                    <ext:RecordField Name="name" />
                                                                                </Fields>
                                                                            </ext:ArrayReader>
                                                                        </Reader>                                                                                                       
                                                                        <SortInfo Field="name" Direction="ASC" />
                                                                    </ext:Store>
                                                                </Store>     
                                                    </ext:ComboBox>
                                              </Items>
                                        </ext:CompositeField>                           

                                        <ext:CompositeField ID="CompositeField4" runat="server" FieldLabel="Email" AnchorHorizontal="95%">
                                              <Items>
                                                    <ext:TextField 
                                                     ID="EMAIL" 
                                                     DataIndex="EMAIL"
                                                     runat="server" 
                                                     Flex="3" />
                                                    <ext:DisplayField ID="DisplayField3" runat="server" Margins="0 0 0 10" Text="Tài khoản:" Flex="2" />
                                                    <ext:TextField 
                                                     ID="TAIKHOAN" 
                                                     DataIndex="TAIKHOAN"
                                                     runat="server" 
                                                     Flex="3" />
                                              </Items>
                                        </ext:CompositeField> 

                                         <ext:TextField 
                                         ID="DIACHI"
                                         DataIndex="DIACHI" 
                                         runat="server" AnchorHorizontal="95%"                          
                                         FieldLabel="Địa chỉ"/>    
                                                    
                                         <ext:TextArea 
                                         ID="GHICHU" 
                                         DataIndex="GHICHU"
                                         runat="server"        
                                         FieldLabel="Ghi chú"
                                         AnchorHorizontal="95%"/>                                                   
                           </Items>
                    </ext:FieldSet>                    
                </Items>
                <BottomBar>
                    <ext:Toolbar ID="Toolbar2" runat="server">  
                        <Items>     
                            <ext:ToolbarFill ID="ToolbarFill1" runat="server" />                                                                         
                            <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <Listeners>
                                    <Click Handler="#{nvForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler });"/>
                                </Listeners>
                            </ext:Button>                                 
                            <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                <Listeners>
                                    <Click Handler="#{nvForm}.reset();" />
                                </Listeners>
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                        </Items>                 
                    </ext:Toolbar>
                </BottomBar>                                
            </ext:FormPanel>
                    </Items>
                    <Listeners>
                        <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
                    </Listeners>
                </ext:Window>                
            </Items>            
        </ext:Viewport>    
    </div>
</body>
</html>
