﻿<%@ Page Language="C#" %>

<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Quan ly Luong nhan vien</title>    
    <style type="text/css">
        h1 {
            font: normal 60px tahoma, arial, verdana;
            color:  #FF6347;
        }
        
        h2 {
            font: normal 40px tahoma, arial, verdana;
            color: tomato;
        }
        
        h2 a {
            text-decoration: none;
            color: #E1E1E1;
        }
        
        .x-window-mc {
            background-color : #F4F4F4 !important;
        }
    </style>
    <script type="text/javascript">
        if (window.top.frames.length !== 0) {
            window.top.location = self.document.location;
        };

        var ck_check = function () {
            var ckvalue = ckRemember.getValue();
            if (ckvalue == true) {
                remember.setValue('true');
            }
            else {
                remember.setValue('');
            }
        };

    </script>
    <script runat="server">        
            private const string SESSION_KEY_LANGUAGE = "CURRENT_LANGUAGE";
            private const string SESSION_KEY_LANGUAGE1 = "en-US";
            
            private void ApplyNewLanguage(System.Globalization.CultureInfo culture)
            {
                QUANLYBANHANG.LanguageManager.CurrentCulture = culture;
                //Keep current language in session
                Session.Add(SESSION_KEY_LANGUAGE, QUANLYBANHANG.LanguageManager.CurrentCulture);
            }

            protected void ApplyNewLanguageAndRefreshPage(System.Globalization.CultureInfo culture)
            {
                ApplyNewLanguage(culture);
                //Refresh the current page to make all control-texts take effect
                Response.Redirect("http://dantri.com.vn");
                //Response.Redirect(Request.Url.AbsoluteUri);
            }
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }        
            
        
    </script>
    <link rel="shortcut icon" href="/Resources/Images/logokh.png" type="image/x-icon"/>
</head>
<body>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
        <Listeners>
            <DocumentReady Handler="Ext.getDoc().on('keydown', function (e) {if (e.getKey() == e.ENTER) {btnLogin.fireEvent('click');}});" />
        </Listeners>
    </ext:ResourceManager>
    <div id="header">
    <h1>Quản lý Thiết bị - Cục Thuế TP Hồ Chí Minh</h1>
    <h2>Copyright © Vacom JSC </h2></div>
    <ext:Window 
        ID="LoginWindow"
        runat="server" 
        Closable="false"
        Resizable="false"
        Height="160" 
        Icon="Lock" 
        Title="Login"
        Draggable="true"
        Width="300"
        Modal="true"
        Layout="fit"
        BodyBorder="false"
        Padding="5">        
        <Items>
                <ext:FormPanel ID="FormPanel1" 
                    runat="server" 
                    FormID="form1"
                    Border="false"
                    Layout="form"
                    BodyBorder="false" 
                    BodyStyle="background:transparent;" 
                    Url='<%# Html.AttributeEncode(Url.Action("Login")) %>'>
                    <Items>
                        <ext:Hidden ID="remember" runat="server" />
                        <ext:TextField 
                            ID="txtUsername" 
                            runat="server" 
                            FieldLabel="Tên" 
                            AllowBlank="false"
                            BlankText="Username is required."                            
                            StyleSpec="text-transform:uppercase;"
                            AnchorHorizontal="100%" />
                         <ext:TextField 
                            ID="txtPassword" 
                            runat="server" 
                            InputType="Password" 
                            FieldLabel="Mật khẩu" 
                            AllowBlank="false" 
                            BlankText="Password is required."
                            AnchorHorizontal="100%" />
                            <ext:Checkbox ID="ckRemember" runat="server" BoxLabel="Ghi nhớ mật khẩu">
                                <Listeners>
                                     <Check Fn="ck_check" />
                                </Listeners>
                            </ext:Checkbox>
                    </Items>
                </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btnLogin" runat="server" Text="Login" Icon="Accept">
                <DirectEvents>
                    <Click 
                        Url="/Account/Login/" 
                        Timeout="9999999"
                        FormID="form1"
                        CleanRequest="true" 
                        Method="POST"
                        Before="Ext.Msg.wait('Đang kiểm tra...', 'Xác thực');"
                        Failure="Ext.Msg.show({
                           title:   'Login Error',
                           msg:     result.errorMessage,
                           buttons: Ext.Msg.OK,
                           icon:    Ext.MessageBox.ERROR
                        });">
                        <EventMask MinDelay="250" />
                        <ExtraParams>
                            <ext:Parameter Name="ReturnUrl" Value="Ext.urlDecode(String(document.location).split('?')[1]).r || '/'" Mode="Raw" />
                        </ExtraParams>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <div id="content"></div>
    <div id="footer" style="background-color:#ADFF2F;clear:both;text-align:center;">
110 Phạm Hồng Thái, phường 7, TP Vũng Tàu, tỉnh Bà Rịa Vũng Tàu </div>
</body>
</html>
