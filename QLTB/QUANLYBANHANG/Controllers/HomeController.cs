﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using QUANLYBANHANG.Models;

namespace QUANLYBANHANG.Controllers
{
    [HandleError]
    public class HomeController : BaseDataController
    {
        System.Resources.ResourceManager rm;
        System.Globalization.CultureInfo ci;
        public ActionResult Index()
        {
            List<DANHMUC> lstmenu = new List<DANHMUC>();
            string theme;
            DANHMUC mnu;
            var menu = (from p in this.DbContext.DANHMUCs
                        select new
                        {
                            p.MA_DANHMUC,
                            p.TEN_DANHMUC_VN,
                            p.TEN_DANHMUC_US,
                            p.TEN_DANHMUC_CN,
                            p.TEN_DANHMUC_JP
                        }).ToList().Take(5);
            var menu1 = (from p in this.DbContext.DANHMUCs
                         select new
                         {
                             p.MA_DANHMUC,
                             p.TEN_DANHMUC_VN,
                             p.TEN_DANHMUC_US,
                             p.TEN_DANHMUC_CN,
                             p.TEN_DANHMUC_JP
                         }).ToList();
            var menu2 = menu1.Except(menu);
            foreach (var mn in menu2)
            {
                mnu = new DANHMUC();
                mnu.MA_DANHMUC = mn.MA_DANHMUC;
                mnu.TEN_DANHMUC_VN = mn.TEN_DANHMUC_VN;
                mnu.TEN_DANHMUC_US = mn.TEN_DANHMUC_US;
                mnu.TEN_DANHMUC_CN = mn.TEN_DANHMUC_CN;
                mnu.TEN_DANHMUC_JP = mn.TEN_DANHMUC_JP;
                lstmenu.Add(mnu);
            }
            this.ViewData["Menu"] = lstmenu;
            var lang = (from p in this.DbContext.ACCOUNTs
                        join d in this.DbContext.NHOM_USERs on p.MA_NHOM equals d.MA_NHOM
                        where p.Username == User.Identity.Name
                        select new
                        {
                            p.Username,
                            p.Language,
                            p.Theme,
                            p.MA_NV,
                            d.TEN_NHOM
                        });
            if (lang.Count() > 0 && !string.IsNullOrEmpty(lang.First().Theme))
            {
                theme = lang.First().Theme;
                this.ViewData["theme"] = theme;
            }

            

            this.ViewData["role"] = lang.First().TEN_NHOM;
            
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
                
        public ContentResult GetNodes(string ma_dm)
        {
            rm = new System.Resources.ResourceManager("Resources.lang",
                         System.Reflection.Assembly.Load("App_GlobalResources")); 
            ci = System.Threading.Thread.CurrentThread.CurrentCulture;           
 
            TreeNodeCollection nodes = new TreeNodeCollection(false);
            if (string.IsNullOrEmpty(ma_dm)) {
                ma_dm = "DM_001";
            }

            var type = (from p in this.DbContext.CT_DANHMUCs
                        where p.MA_DANHMUC == ma_dm
                        select new
                        {
                            p.KIEU_DM_VN,
                            p.KIEU_DM_EN,
                            p.KIEU_DM_CN,
                            p.KIEU_DM_JP
                        }).Distinct().ToList();
            foreach (var mn in type) {
                if (ci.Name == "vi-VN")
                {
                    var query1 = from c in this.DbContext.DANHMUCs
                                 join d in this.DbContext.CT_DANHMUCs on c.MA_DANHMUC equals d.MA_DANHMUC
                                 join e in this.DbContext.DANHMUC_CONs on d.MA_DMC equals e.MA_DMC
                                 where c.MA_DANHMUC == ma_dm && d.KIEU_DM_VN == mn.KIEU_DM_VN
                                 select new
                                 {
                                     d.MA_DMC,
                                     e.TEN_DMC_VN,
                                     e.TEN_DMC_EN,
                                     e.TEN_DMC_CN,
                                     e.TEN_DMC_JP,
                                     e.LINK_DMC
                                 };
                    TreeNode asyncNode = new TreeNode();
                    asyncNode.Text = mn.KIEU_DM_VN;
                    asyncNode.NodeID = mn.KIEU_DM_VN;
                    asyncNode.Expanded = true;
                    asyncNode.Icon = Icon.FolderTable;

                    foreach (var item1 in query1)
                    {
                        TreeNode node = new TreeNode();
                        node.NodeID = item1.MA_DMC;
                        node.Text = item1.TEN_DMC_VN;                        
                        node.Leaf = true;
                        node.Icon = Icon.Cmy;
                        ConfigItem configItem = new ConfigItem();
                        configItem.Name = "url";
                        configItem.Mode = ParameterMode.Value;
                        configItem.Value = item1.LINK_DMC;
                        node.CustomAttributes.Add(configItem);
                        node.Listeners.Click.Handler = "Northwind.addTab({ title : this.text, url : this.attributes.url , icon : this.iconCls });";
                        asyncNode.Nodes.Add(node);
                    }
                    nodes.Add(asyncNode);
                }            
                else if(ci.Name == "en-US")
                {
                    var query1 = from c in this.DbContext.DANHMUCs
                                 join d in this.DbContext.CT_DANHMUCs on c.MA_DANHMUC equals d.MA_DANHMUC
                                 join e in this.DbContext.DANHMUC_CONs on d.MA_DMC equals e.MA_DMC
                                 where c.MA_DANHMUC == ma_dm && d.KIEU_DM_EN == mn.KIEU_DM_EN
                                 select new
                                 {
                                     d.MA_DMC,
                                     e.TEN_DMC_VN,
                                     e.TEN_DMC_EN,
                                     e.TEN_DMC_CN,
                                     e.TEN_DMC_JP,
                                     e.LINK_DMC
                                 };
                    TreeNode asyncNode = new TreeNode();
                    asyncNode.Text = mn.KIEU_DM_EN;
                    asyncNode.NodeID = mn.KIEU_DM_EN;
                    asyncNode.Expanded = true;
                    asyncNode.Icon = Icon.BookOpenMark;

                    foreach (var item1 in query1)
                    {
                        TreeNode node = new TreeNode();
                        node.NodeID = item1.MA_DMC;
                        node.Text = item1.TEN_DMC_EN;
                        node.Leaf = true;
                        node.Icon = Icon.FolderTable;
                        ConfigItem configItem = new ConfigItem();
                        configItem.Name = "url";
                        configItem.Mode = ParameterMode.Value;
                        configItem.Value = item1.LINK_DMC;
                        node.CustomAttributes.Add(configItem);
                        node.Listeners.Click.Handler = "Northwind.addTab({ title : this.text, url : this.attributes.url , icon : this.iconCls });";
                        asyncNode.Nodes.Add(node);
                    }
                    nodes.Add(asyncNode);
                }
            }
            return Content(nodes.ToJson());
        }        
    }

}
