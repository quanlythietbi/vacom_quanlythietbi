﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QUANLYBANHANG.Models;

namespace QUANLYBANHANG.Controllers
{
    public class BaseDataController : Controller
    {
        //
        // GET: /BaseData/

        QLBHDataContext dbContext;

        public BaseDataController()
        {
            this.dbContext = new QLBHDataContext();
        }

        public QLBHDataContext DbContext
        {
            get { return this.dbContext; }
        }

    }
}
