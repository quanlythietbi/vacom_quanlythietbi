﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using QUANLYBANHANG.Models;
using Ext.Net;
using System.Data.Linq.SqlClient;
using VACOMLIB;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using OfficeOpenXml;

namespace QUANLYBANHANG.Controllers
{
    public class DanhmucController : BaseDataController
    {
        //
        // GET: /Danhmuc/

        public ContentResult GetMenuPanel()
        {
            ContentResult cr = new ContentResult();
            Button btn;            
            System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Resources.lang",
                         System.Reflection.Assembly.Load("App_GlobalResources")); 
            System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;           

            Panel gr = new Panel();
            gr.ID = "menu";
            gr.Border = false;
            gr.AutoScroll = true;
                 
            var menu = (from p in this.DbContext.DANHMUCs                        
                        select new
                        {
                            p.MA_DANHMUC,
                            p.TEN_DANHMUC_VN,
                            p.TEN_DANHMUC_US,
                            p.TEN_DANHMUC_CN,
                            p.TEN_DANHMUC_JP,
                            p.TEN_ICON
                        }).ToList().Take(5);
            foreach (var dm in menu) {
                btn = new Button();
                btn.ID = dm.MA_DANHMUC;
                if (ci.Name == "ja-JP")
                    btn.Text = dm.TEN_DANHMUC_JP;
                else if (ci.Name == "en-US")
                    btn.Text = dm.TEN_DANHMUC_US;
                else if (ci.Name == "zh-CN")
                    btn.Text = dm.TEN_DANHMUC_CN;
                else
                    btn.Text = dm.TEN_DANHMUC_VN;
                btn.Width = 230;
                btn.IconCls = dm.TEN_ICON;
                btn.Scale = ButtonScale.Large;
                //float d = (float)(1 / menu.Count);
                //btn.RowHeight = 0.2;
                btn.Listeners.Click.Fn = "loadChildMenu";
                gr.Add(btn);
            }           

            string script = @"<script>{0}</script>";
            cr.Content = string.Format(script, gr.ToScript(RenderMode.AddTo, "MenuPanel"));

            return cr;
        }

        #region "Danh mục đơn vị"

        public ActionResult Dm_Donvi()
        {
            return View();
        }

        public AjaxStoreResult dsDonvi(string txtfilter, int start, int limit)
        {
            var query = (from c in this.DbContext.DM_DONVIs
                         where SqlMethods.Like(c.TEN_DV, "%" + LIB.ProcessStrVal(txtfilter) + "%")
                         orderby c.MA_DV ascending
                         select new
                         {
                             c.MA_DV,
                             c.TEN_DV,
                             c.SDT,
                             c.FAX,
                             c.DIACHI,
                             c.TEN_TAT,
                             c.MST,
                             c.EMAIL,
                             c.GHICHU
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveDonvi(FormCollection values)
        {
            DM_DONVI dm;
            AjaxFormResult response = new AjaxFormResult();
            var query = from c in this.DbContext.DM_DONVIs
                        select c;
            try
            {
                if (string.IsNullOrEmpty(values["id"]))
                {
                    foreach (DM_DONVI p in query)
                    {
                        if (p.MA_DV.Equals(values["MA_CV"]))
                        {
                            response.Success = false;
                            response.ExtraParams["msg"] = "Mã đơn vị đã tồn tại!";
                            return response;
                        }
                    }
                    dm = new DM_DONVI();
                    dm.MA_DV = values["MA_DV"];
                    dm.TEN_DV = values["TEN_DV"];
                    dm.DIACHI = values["DIACHI"];
                    dm.TEN_TAT = values["TEN_TAT"];
                    dm.MST = values["MST"];
                    dm.SDT = values["SDT"];
                    dm.FAX = values["FAX"];
                    dm.EMAIL = values["EMAIL"];
                    dm.GHICHU = values["GHICHU"];

                    this.DbContext.DM_DONVIs.InsertOnSubmit(dm);
                    this.DbContext.SubmitChanges();
                }
                else
                {
                    if (values["MA_DV"].Equals(values["id"]))
                    {
                        dm = (from p in this.DbContext.DM_DONVIs
                              where p.MA_DV == values["id"]
                              select p).First();

                        dm.TEN_DV = values["TEN_DV"];
                        dm.DIACHI = values["DIACHI"];
                        dm.SDT = values["SDT"];
                        dm.TEN_TAT = values["TEN_TAT"];
                        dm.MST = values["MST"];
                        dm.FAX = values["FAX"];
                        dm.EMAIL = values["EMAIL"];
                        dm.GHICHU = values["GHICHU"];

                        this.DbContext.SubmitChanges();
                    }
                    else
                    {
                        foreach (DM_DONVI p in query)
                        {
                            if (p.MA_DV.Equals(values["MA_DV"]))
                            {
                                response.Success = false;
                                response.ExtraParams["msg"] = "Mã đơn vị đã tồn tại!";
                                return response;
                            }
                        }
                        this.DbContext.ExecuteCommand("UPDATE DM_DONVI SET MA_DV = {0} WHERE MA_DV = {1}", values["MA_DV"], values["id"]);
                        dm = (from p in this.DbContext.DM_DONVIs
                              where p.MA_DV == values["MA_DV"]
                              select p).First();

                        dm.TEN_DV = values["TEN_DV"];
                        dm.DIACHI = values["DIACHI"];
                        dm.SDT = values["SDT"];
                        dm.TEN_TAT = values["TEN_TAT"];
                        dm.MST = values["MST"];
                        dm.FAX = values["FAX"];
                        dm.EMAIL = values["EMAIL"];
                        dm.GHICHU = values["GHICHU"];
                        this.DbContext.SubmitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaDonvi(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.DM_DONVIs
                          where c.MA_DV == id
                          select c).First();
                this.DbContext.DM_DONVIs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Danh mục vị trí lắp đặt"

        public ActionResult Dm_Vitri()
        {
            return View();
        }

        public AjaxStoreResult dsVitri(string txtfilter, int start, int limit)
        {
            var query = (from c in this.DbContext.DM_VITRI_LAPDATs
                         where SqlMethods.Like(c.TEN_VT, "%" + LIB.ProcessStrVal(txtfilter) + "%")
                         orderby c.MA_VT ascending
                         select new
                         {
                             c.MA_VT,
                             c.TEN_VT,
                             c.SDT,
                             c.GHICHU
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveVitri(FormCollection values)
        {
            DM_VITRI_LAPDAT dm;
            AjaxFormResult response = new AjaxFormResult();
            
            try
            {
                if (string.IsNullOrEmpty(values["id"]))
                {
                    dm = new DM_VITRI_LAPDAT();
                    dm.TEN_VT = values["TEN_VT"];
                    dm.SDT = values["SDT"];
                    dm.GHICHU = values["GHICHU"];

                    this.DbContext.DM_VITRI_LAPDATs.InsertOnSubmit(dm);
                    this.DbContext.SubmitChanges();
                }
                else
                {
                    dm = (from p in this.DbContext.DM_VITRI_LAPDATs
                          where p.MA_VT == Convert.ToInt32(values["id"])
                          select p).First();

                    dm.TEN_VT = values["TEN_VT"];
                    dm.SDT = values["SDT"];
                    dm.GHICHU = values["GHICHU"];

                    this.DbContext.SubmitChanges();
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaVitri(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.DM_VITRI_LAPDATs
                          where c.MA_VT == Convert.ToInt32(id)
                          select c).First();
                this.DbContext.DM_VITRI_LAPDATs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Danh mục chủng loại"

        public ActionResult Dm_Chungloai()
        {
            return View();
        }

        public AjaxStoreResult dsChungloai(string txtfilter, int start, int limit)
        {
            var query = (from c in this.DbContext.DM_CHUNGLOAIs
                         where SqlMethods.Like(c.TEN_CL, "%" + LIB.ProcessStrVal(txtfilter) + "%")
                         orderby c.MA_CL ascending
                         select new
                         {
                             c.MA_CL,
                             c.TEN_CL,
                             c.GHICHU
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveChungloai(FormCollection values)
        {
            DM_CHUNGLOAI dm;
            AjaxFormResult response = new AjaxFormResult();
            var query = from c in this.DbContext.DM_CHUNGLOAIs
                        select c;
            try
            {
                if (string.IsNullOrEmpty(values["id"]))
                {
                    foreach (DM_CHUNGLOAI p in query)
                    {
                        if (p.MA_CL.Equals(values["MA_CL"]))
                        {
                            response.Success = false;
                            response.ExtraParams["msg"] = "Mã chủng loại đã tồn tại!";
                            return response;
                        }
                    }
                    dm = new DM_CHUNGLOAI();
                    dm.MA_CL = values["MA_CL"];
                    dm.TEN_CL = values["TEN_CL"];
                    dm.GHICHU = values["GHICHU"];

                    this.DbContext.DM_CHUNGLOAIs.InsertOnSubmit(dm);
                    this.DbContext.SubmitChanges();
                }
                else
                {
                    if (values["MA_CL"].Equals(values["id"]))
                    {
                        dm = (from p in this.DbContext.DM_CHUNGLOAIs
                              where p.MA_CL == values["id"]
                              select p).First();

                        dm.TEN_CL = values["TEN_CL"];
                        dm.GHICHU = values["GHICHU"];

                        this.DbContext.SubmitChanges();
                    }
                    else
                    {
                        foreach (DM_CHUNGLOAI p in query)
                        {
                            if (p.MA_CL.Equals(values["MA_CL"]))
                            {
                                response.Success = false;
                                response.ExtraParams["msg"] = "Mã chủng loại đã tồn tại!";
                                return response;
                            }
                        }
                        this.DbContext.ExecuteCommand("UPDATE DM_CHUNGLOAI SET MA_CL = {0} WHERE MA_CL = {1}", values["MA_CL"], values["id"]);
                        dm = (from p in this.DbContext.DM_CHUNGLOAIs
                              where p.MA_CL == values["MA_CL"]
                              select p).First();

                        dm.TEN_CL = values["TEN_CL"];
                        dm.GHICHU = values["GHICHU"];
                        this.DbContext.SubmitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaChungloai(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.DM_CHUNGLOAIs
                          where c.MA_CL == id
                          select c).First();
                this.DbContext.DM_CHUNGLOAIs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Danh mục kiểu"

        public ActionResult Dm_Kieu()
        {
            return View();
        }

        public AjaxStoreResult dsKieu(string txtfilter, int start, int limit)
        {
            var query = (from c in this.DbContext.DM_KIEUs
                         join d in this.DbContext.DM_CHUNGLOAIs on c.MA_CL equals d.MA_CL into ps
                         from d in ps.DefaultIfEmpty()
                         where SqlMethods.Like(c.TEN_KIEU, "%" + LIB.ProcessStrVal(txtfilter) + "%")
                         orderby d.TEN_CL ascending
                         select new
                         {
                             c.MA_CL,
                             c.MA_KIEU,
                             c.TEN_KIEU,
                             d.TEN_CL,
                             c.GHICHU
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveKieu(FormCollection values)
        {
            DM_KIEU dm;
            AjaxFormResult response = new AjaxFormResult();

            try
            {
                if (string.IsNullOrEmpty(values["id"]))
                {
                    dm = new DM_KIEU();
                    dm.TEN_KIEU = values["TEN_KIEU"];
                    dm.MA_CL = values["MA_CL_Value"];
                    dm.GHICHU = values["GHICHU"];

                    this.DbContext.DM_KIEUs.InsertOnSubmit(dm);
                    this.DbContext.SubmitChanges();
                }
                else
                {
                    dm = (from p in this.DbContext.DM_KIEUs
                          where p.MA_KIEU == Convert.ToInt32(values["id"])
                          select p).First();

                    dm.TEN_KIEU = values["TEN_KIEU"];
                    dm.MA_CL = values["MA_CL_Value"];
                    dm.GHICHU = values["GHICHU"];

                    this.DbContext.SubmitChanges();
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaKieu(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.DM_KIEUs
                          where c.MA_KIEU == Convert.ToInt32(id)
                          select c).First();
                this.DbContext.DM_KIEUs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Danh mục Trạng Thái"

        public ActionResult Dm_Trangthai()
        {
            return View();
        }

        public AjaxStoreResult dsTrangthai(string txtfilter, int start, int limit)
        {
            var query = (from c in this.DbContext.DM_TRANGTHAIs
                         where SqlMethods.Like(c.TEN_TT, "%" + LIB.ProcessStrVal(txtfilter) + "%")
                         orderby c.MA_TT ascending
                         select new
                         {
                             c.MA_TT,
                             c.TEN_TT,
                             c.GHICHU
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveTrangthai(FormCollection values)
        {
            DM_TRANGTHAI dm;
            AjaxFormResult response = new AjaxFormResult();
           
            try
            {
                if (string.IsNullOrEmpty(values["id"]))
                {
                    dm = new DM_TRANGTHAI();
                    dm.TEN_TT = values["TEN_TT"];
                    dm.GHICHU = values["GHICHU"];

                    this.DbContext.DM_TRANGTHAIs.InsertOnSubmit(dm);
                    this.DbContext.SubmitChanges();
                }
                else
                {
                    dm = (from p in this.DbContext.DM_TRANGTHAIs
                          where p.MA_TT == Convert.ToInt32(values["id"])
                          select p).First();

                    dm.TEN_TT = values["TEN_TT"];
                    dm.GHICHU = values["GHICHU"];

                    this.DbContext.SubmitChanges();
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaTrangthai(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.DM_TRANGTHAIs
                          where c.MA_TT == Convert.ToInt32(id)
                          select c).First();
                this.DbContext.DM_TRANGTHAIs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion 



        //#region "Danh mục khách hàng"

        //public ActionResult Dm_Chuxe()
        //{
        //    return View();
        //}

        //public AjaxStoreResult dsChuxe(string txtfilter, int start, int limit)
        //{
        //    var query = (from c in this.DbContext.DM_CHUXEs
        //                 where SqlMethods.Like(c.TEN_CX, "%" + LIB.ProcessStrVal(txtfilter) + "%")
        //                 orderby c.MA_CX ascending
        //                 select new
        //                 {
        //                     c.MA_CX,
        //                     c.TEN_CX,
        //                     c.NGAY_CAP,
        //                     c.GIOI_TINH,
        //                     c.NGAY_SINH,
        //                     c.SO_CMND,
        //                     c.NOI_CAP,
        //                     c.DK_HKTT,
        //                     c.VO,
        //                     c.NGAY_SINH_VO,
        //                     c.CMND_VO,
        //                     c.NOI_CAP_VO,
        //                     c.NGAY_CAP_VO,
        //                     c.DK_HKTT_VO,
        //                     c.CN_KETHON,
        //                     c.GHICHU
        //                 });
        //    int total = query.ToList().Count;

        //    query = query.Skip(start).Take(limit);
        //    return new AjaxStoreResult(query, total);
        //}

        //public AjaxFormResult saveChuxe(FormCollection values)
        //{
        //    DM_CHUXE dm;
        //    AjaxFormResult response = new AjaxFormResult();
        //    var query = from c in this.DbContext.DM_CHUXEs
        //                select c;
        //    try
        //    {
        //        if (string.IsNullOrEmpty(values["id"]))
        //        {
        //            foreach (DM_CHUXE p in query)
        //            {
        //                if (p.MA_CX.Equals(values["MA_CX"]))
        //                {
        //                    response.Success = false;
        //                    response.ExtraParams["msg"] = "Mã khách hàng đã tồn tại!";
        //                    return response;
        //                }
        //            } 
        //            dm = new DM_CHUXE();
        //            dm.MA_CX = values["MA_CX"];
        //            dm.TEN_CX = values["TEN_CX"];
        //            dm.SO_CMND = values["SO_CMND"];
        //            if (values["grGioitinh_GROUP"] == "nam")
        //                dm.GIOI_TINH = "nam";
        //            else
        //                dm.GIOI_TINH = "nữ";
        //            dm.NGAY_SINH = Convert.ToDateTime(values["NGAY_SINH"]);
        //            dm.NGAY_CAP = Convert.ToDateTime(values["NGAY_CAP"]);
        //            dm.NOI_CAP = values["NOI_CAP"];
        //            dm.DK_HKTT = values["DK_HKTT"];
        //            dm.VO = values["VO"];
        //            dm.NGAY_SINH_VO = Convert.ToDateTime(values["NGAY_SINH_VO"]);
        //            dm.CMND_VO = values["CMND_VO"];
        //            dm.NGAY_CAP_VO = Convert.ToDateTime(values["NGAY_CAP_VO"]);
        //            dm.NOI_CAP_VO = values["NOI_CAP_VO"];
        //            dm.DK_HKTT_VO = values["DK_HKTT_VO"];
        //            dm.CN_KETHON = values["CN_KETHON"];
        //            dm.GHICHU = values["GHICHU"];

        //            this.DbContext.DM_CHUXEs.InsertOnSubmit(dm);
        //            this.DbContext.SubmitChanges();
        //        }
        //        else
        //        {
        //            if (values["MA_CX"].Equals(values["id"]))
        //            {
        //                dm = (from p in this.DbContext.DM_CHUXEs
        //                      where p.MA_CX == values["id"]
        //                      select p).First();

        //                dm.TEN_CX = values["TEN_CX"];
        //                dm.SO_CMND = values["SO_CMND"];
        //                if (values["grGioitinh_GROUP"] == "nam")
        //                    dm.GIOI_TINH = "nam";
        //                else
        //                    dm.GIOI_TINH = "nữ";
        //                dm.NGAY_SINH = Convert.ToDateTime(values["NGAY_SINH"]);
        //                dm.NGAY_CAP = Convert.ToDateTime(values["NGAY_CAP"]);
        //                dm.NOI_CAP = values["NOI_CAP"];
        //                dm.DK_HKTT = values["DK_HKTT"];
        //                dm.VO = values["VO"];
        //                dm.NGAY_SINH_VO = Convert.ToDateTime(values["NGAY_SINH_VO"]);
        //                dm.CMND_VO = values["CMND_VO"];
        //                dm.NGAY_CAP_VO = Convert.ToDateTime(values["NGAY_CAP_VO"]);
        //                dm.NOI_CAP_VO = values["NOI_CAP_VO"];
        //                dm.DK_HKTT_VO = values["DK_HKTT_VO"];
        //                dm.CN_KETHON = values["CN_KETHON"];
        //                dm.GHICHU = values["GHICHU"];

        //                this.DbContext.SubmitChanges();
        //            }
        //            else
        //            {
        //                foreach (DM_CHUXE p in query)
        //                {
        //                    if (p.MA_CX.Equals(values["MA_CX"]))
        //                    {
        //                        response.Success = false;
        //                        response.ExtraParams["msg"] = "Mã chủ xe đã tồn tại!";
        //                        return response;
        //                    }
        //                }
        //                this.DbContext.ExecuteCommand("UPDATE DM_CHUXE SET MA_CX = {0} WHERE MA_CX = {1}", values["MA_CX"], values["id"]);
        //                dm = (from p in this.DbContext.DM_CHUXEs
        //                      where p.MA_CX == values["MA_CX"]
        //                      select p).First();

        //                dm.TEN_CX = values["TEN_CX"];
        //                dm.SO_CMND = values["SO_CMND"];
        //                if (values["grGioitinh_GROUP"] == "nam")
        //                    dm.GIOI_TINH = "nam";
        //                else
        //                    dm.GIOI_TINH = "nữ";
        //                dm.NGAY_SINH = Convert.ToDateTime(values["NGAY_SINH"]);
        //                dm.NGAY_CAP = Convert.ToDateTime(values["NGAY_CAP"]);
        //                dm.NOI_CAP = values["NOI_CAP"];
        //                dm.DK_HKTT = values["DK_HKTT"];
        //                dm.VO = values["VO"];
        //                dm.NGAY_SINH_VO = Convert.ToDateTime(values["NGAY_SINH_VO"]);
        //                dm.CMND_VO = values["CMND_VO"];
        //                dm.NGAY_CAP_VO = Convert.ToDateTime(values["NGAY_CAP_VO"]);
        //                dm.NOI_CAP_VO = values["NOI_CAP_VO"];
        //                dm.DK_HKTT_VO = values["DK_HKTT_VO"];
        //                dm.CN_KETHON = values["CN_KETHON"];
        //                dm.GHICHU = values["GHICHU"];
        //                this.DbContext.SubmitChanges();
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        response.Success = false;
        //        response.ExtraParams["msg"] = e.ToString();
        //    }
        //    return response;
        //}

        //public AjaxResult xoaChuxe(string id)
        //{
        //    AjaxResult response = new AjaxResult();
        //    try
        //    {
        //        var nv = (from c in this.DbContext.DM_CHUXEs
        //                  where c.MA_CX == id
        //                  select c).First();
        //        this.DbContext.DM_CHUXEs.DeleteOnSubmit(nv);
        //        this.DbContext.SubmitChanges();
        //    }
        //    catch (Exception e)
        //    {
        //        response.ErrorMessage = e.ToString();
        //    }
        //    return response;
        //}

        //public AjaxStoreResult dsXe(string txtfilter, int start, int limit)
        //{
        //    var query = (from c in this.DbContext.DM_XEs
        //                 join d in this.DbContext.DM_CHUXEs on c.MA_CX equals d.MA_CX
        //                 where c.MA_CX == txtfilter
        //                 orderby c.MA_CX ascending
        //                 select new
        //                 {
        //                     c.MA_CX,
        //                     c.MA_XE,
        //                     d.TEN_CX,
        //                     c.BIEN_SO,
        //                     c.LOAI_XE,
        //                     c.MAU,
        //                     c.NHAN_HIEU,
        //                     c.SO_KHUNG,
        //                     c.SO_MAY,
        //                     c.SO_DK,
        //                     c.NGAY_CAP,
        //                     c.NOI_CAP,
        //                     c.GHICHU
        //                 });
        //    int total = query.ToList().Count;

        //    query = query.Skip(start).Take(limit);
        //    return new AjaxStoreResult(query, total);
        //}

        //public ActionResult Dm_Xe()
        //{
        //    return View();
        //}

        //public AjaxStoreResult dsXe_Dm(string txtfilter, int start, int limit)
        //{
        //    var query = (from c in this.DbContext.DM_XEs
        //                 join d in this.DbContext.DM_CHUXEs on c.MA_CX equals d.MA_CX
        //                 where SqlMethods.Like(c.BIEN_SO, "%" + LIB.ProcessStrVal(txtfilter) + "%")
        //                 || SqlMethods.Like(c.SO_KHUNG, "%" + LIB.ProcessStrVal(txtfilter) + "%")
        //                 || SqlMethods.Like(c.SO_MAY, "%" + LIB.ProcessStrVal(txtfilter) + "%")
        //                 || SqlMethods.Like(d.TEN_CX, "%" + LIB.ProcessStrVal(txtfilter) + "%")
        //                 orderby c.MA_CX ascending
        //                 select new
        //                 {
        //                     c.MA_CX,
        //                     c.MA_XE,
        //                     d.TEN_CX,
        //                     c.BIEN_SO,
        //                     c.LOAI_XE,
        //                     c.MAU,
        //                     c.NHAN_HIEU,
        //                     c.SO_KHUNG,
        //                     c.SO_MAY,
        //                     c.SO_DK,
        //                     c.NGAY_CAP,
        //                     c.NOI_CAP,
        //                     c.GHICHU
        //                 });
        //    int total = query.ToList().Count;

        //    query = query.Skip(start).Take(limit);
        //    return new AjaxStoreResult(query, total);
        //}

        //public AjaxFormResult saveXe(FormCollection values)
        //{
        //    DM_XE dm;
        //    AjaxFormResult response = new AjaxFormResult();
        //    var query = from c in this.DbContext.DM_CHUXEs
        //                select c;
        //    try
        //    {
        //        if (string.IsNullOrEmpty(values["id"]))
        //        {
        //            dm = new DM_XE();
        //            dm.MA_CX = values["CHUXE_Value"];
        //            dm.BIEN_SO = values["BIEN_SO"];
        //            dm.LOAI_XE = values["LOAI_XE"];
        //            dm.NHAN_HIEU = values["NHAN_HIEU"];
        //            dm.SO_KHUNG = values["SO_KHUNG"];
        //            dm.SO_MAY = values["SO_MAY"];
        //            dm.SO_DK = values["SO_DK"];
        //            dm.MAU = values["MAU"];
        //            if (!string.IsNullOrEmpty(values["NGAY_CAP"]))
        //                dm.NGAY_CAP = Convert.ToDateTime(values["NGAY_CAP"]);
        //            dm.NOI_CAP = values["NOI_CAP"];
        //            dm.GHICHU = values["GHICHU"];
        //            dm.TRANGTHAI = "Chưa bán";

        //            this.DbContext.DM_XEs.InsertOnSubmit(dm);
        //            this.DbContext.SubmitChanges();
        //        }
        //        else
        //        {
        //            dm = (from p in this.DbContext.DM_XEs
        //                  where p.MA_XE == Convert.ToInt32(values["id"])
        //                  select p).First();

        //            dm.MA_CX = values["CHUXE_Value"];
        //            dm.BIEN_SO = values["BIEN_SO"];
        //            dm.LOAI_XE = values["LOAI_XE"];
        //            dm.NHAN_HIEU = values["NHAN_HIEU"];
        //            dm.SO_KHUNG = values["SO_KHUNG"];
        //            dm.SO_MAY = values["SO_MAY"];
        //            dm.SO_DK = values["SO_DK"];
        //            dm.MAU = values["MAU"];
        //            if (!string.IsNullOrEmpty(values["NGAY_CAP"]))
        //                dm.NGAY_CAP = Convert.ToDateTime(values["NGAY_CAP"]);
        //            dm.NOI_CAP = values["NOI_CAP"];
        //            dm.GHICHU = values["GHICHU"];

        //            this.DbContext.SubmitChanges();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        response.Success = false;
        //        response.ExtraParams["msg"] = e.ToString();
        //    }
        //    return response;
        //}

        //public AjaxFormResult saveXe1(FormCollection values)
        //{
        //    DM_XE dm;
        //    AjaxFormResult response = new AjaxFormResult();
        //    var query = (from c in this.DbContext.DM_CHUXEs
        //                join d in this.DbContext.ACCOUNTs on c.MA_CX equals d.MA_NV
        //                where d.Username == User.Identity.Name
        //                select c).First();
        //    try
        //    {
        //        if (string.IsNullOrEmpty(values["id2"]))
        //        {
        //            dm = new DM_XE();
        //            dm.MA_CX = query.MA_CX;
        //            dm.BIEN_SO = values["BIEN_SO"];
        //            dm.LOAI_XE = values["LOAI_XE"];
        //            dm.NHAN_HIEU = values["NHAN_HIEU"];
        //            dm.SO_KHUNG = values["SO_KHUNG"];
        //            dm.SO_MAY = values["SO_MAY"];
        //            dm.SO_DK = values["SO_DK"];
        //            dm.MAU = values["MAU"];
        //            if (!string.IsNullOrEmpty(values["NGAY_CAP_XE"]))
        //                dm.NGAY_CAP = Convert.ToDateTime(values["NGAY_CAP_XE"]);
        //            dm.NOI_CAP = values["NOI_CAP_XE"];
        //            dm.GHICHU = values["GHICHU_XE"];
        //            dm.TRANGTHAI = "Chưa bán";

        //            this.DbContext.DM_XEs.InsertOnSubmit(dm);
        //            this.DbContext.SubmitChanges();
        //        }
        //        else
        //        {
        //            dm = (from p in this.DbContext.DM_XEs
        //                  where p.MA_XE == Convert.ToInt32(values["id2"])
        //                  select p).First();

        //            dm.MA_CX = query.MA_CX;
        //            dm.BIEN_SO = values["BIEN_SO"];
        //            dm.LOAI_XE = values["LOAI_XE"];
        //            dm.NHAN_HIEU = values["NHAN_HIEU"];
        //            dm.SO_KHUNG = values["SO_KHUNG"];
        //            dm.SO_MAY = values["SO_MAY"];
        //            dm.SO_DK = values["SO_DK"];
        //            dm.MAU = values["MAU"];
        //            if (!string.IsNullOrEmpty(values["NGAY_CAP_XE"]))
        //                dm.NGAY_CAP = Convert.ToDateTime(values["NGAY_CAP_XE"]);
        //            dm.NOI_CAP = values["NOI_CAP_XE"];
        //            dm.GHICHU = values["GHICHU_XE"];

        //            this.DbContext.SubmitChanges();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        response.Success = false;
        //        response.ExtraParams["msg"] = e.ToString();
        //    }
        //    return response;
        //}

        //public AjaxResult xoaXe(string id)
        //{
        //    AjaxResult response = new AjaxResult();
        //    try
        //    {
        //        var nv = (from c in this.DbContext.DM_XEs
        //                  where c.MA_XE == Convert.ToInt32(id)
        //                  select c).First();
        //        this.DbContext.DM_XEs.DeleteOnSubmit(nv);
        //        this.DbContext.SubmitChanges();
        //    }
        //    catch (Exception e)
        //    {
        //        response.ErrorMessage = e.ToString();
        //    }
        //    return response;
        //}

        //public AjaxFormResult saveMuaxe(FormCollection values)
        //{
        //    DM_XE dm;
        //    DM_BENMUA bm;
        //    AjaxFormResult response = new AjaxFormResult();
        //    var query = from c in this.DbContext.DM_BENMUAs
        //                join d in this.DbContext.DM_XEs on c.MA_BM equals d.MA_BM
        //                join e in this.DbContext.ACCOUNTs on d.MA_CX equals e.MA_NV
        //                where e.Username == User.Identity.Name
        //                select c;
        //    try
        //    {
        //        foreach (var s in query)
        //        {
        //            this.DbContext.DM_BENMUAs.DeleteOnSubmit(s);
        //            this.DbContext.SubmitChanges();
        //        }
        //        bm = new DM_BENMUA();
        //        bm.TEN_BM = values["TEN_BM"];
        //        if (!string.IsNullOrEmpty(values["NGAY_SINH_MUA"]))
        //            bm.NGAY_SINH = Convert.ToDateTime(values["NGAY_SINH_MUA"]);
        //        bm.SO_CMND = values["SO_CMND_MUA"];
        //        if (!string.IsNullOrEmpty(values["NGAY_CAP_MUA"]))
        //            bm.NGAY_CAP = Convert.ToDateTime(values["NGAY_CAP_MUA"]);
        //        bm.NOI_CAP = values["NOI_CAP_MUA"];
        //        bm.DK_HKTT = values["DK_HKTT_MUA"];
        //        bm.GHICHU = values["GHICHU_MUA"];
        //        this.DbContext.DM_BENMUAs.InsertOnSubmit(bm);
        //        this.DbContext.SubmitChanges();

        //        var q = (from p in this.DbContext.DM_BENMUAs
        //                 orderby p.MA_BM descending
        //                 select p).First();

        //        dm = (from p in this.DbContext.DM_XEs
        //              where p.MA_XE == Convert.ToInt32(values["id1"])
        //              select p).First();
        //        dm.MA_BM = q.MA_BM;
        //        dm.TRANGTHAI = "Đã bán";
        //        this.DbContext.SubmitChanges();
        //    }
        //    catch (Exception e)
        //    {
        //        response.Success = false;
        //        response.ExtraParams["msg"] = e.ToString();
        //    }
        //    return response;
        //}

        //public ActionResult InPhieu()
        //{
        //    ViewData["ma_xe"] = HttpContext.Request["ma_xe"] ?? "";
        //    return View();
        //}

        //public ActionResult getPhieu(string ma_xe)
        //{
        //    this.HttpContext.Session["ReportName"] = "In_HD.rpt";
        //    this.HttpContext.Session["ma_xe"] = ma_xe;
        //    return RedirectToAction("In_Phieu", "Danhmuc");
        //}

        //public void In_Phieu()
        //{
        //    try
        //    {

        //        string strReportName = System.Web.HttpContext.Current.Session["ReportName"].ToString();

        //        string ma_xe = System.Web.HttpContext.Current.Session["ma_xe"].ToString();

        //        // Setting ReportName    
                
        //        ReportClass report = new ReportClass();
        //        string strRptPath = System.Web.HttpContext.Current.Server.MapPath("/Resources/Report/") + strReportName;
        //        report.FileName = strRptPath;
        //        report.Load();

        //        var query = (from p in this.DbContext.DM_XEs
        //                     join d in this.DbContext.DM_CHUXEs on p.MA_CX equals d.MA_CX
        //                     join e in this.DbContext.DM_BENMUAs on p.MA_BM equals e.MA_BM
        //                     where p.MA_XE == Convert.ToInt32(ma_xe)
        //                     select new
        //                     {
        //                         p.MA_XE,
        //                         d.TEN_CX,
        //                         d.SO_CMND,
        //                         d.NGAY_SINH,
        //                         d.NGAY_CAP,
        //                         d.NOI_CAP,
        //                         d.VO,
        //                         d.CMND_VO,
        //                         d.NGAY_CAP_VO,
        //                         d.NOI_CAP_VO,
        //                         d.NGAY_SINH_VO,
        //                         d.DK_HKTT,
        //                         e.TEN_BM,
        //                         NGAY_SINH_MUA = e.NGAY_SINH,
        //                         CMND_MUA = e.SO_CMND,
        //                         NGAY_CAP_MUA = e.NGAY_CAP,
        //                         NOI_CAP_MUA = e.NOI_CAP,
        //                         HKTT_MUA = e.DK_HKTT,
        //                         p.BIEN_SO,
        //                         p.LOAI_XE,
        //                         p.NHAN_HIEU,
        //                         p.MAU,
        //                         p.SO_KHUNG,
        //                         p.SO_MAY,
        //                         p.SO_DK,
        //                         NOI_CAP_DK = p.NOI_CAP,
        //                         NGAY_CAP_DK = p.NGAY_CAP
        //                     }).First();

        //        report.SetParameterValue("@ngay", DateTime.Now.Day);
        //        report.SetParameterValue("@thang", DateTime.Now.Month);
        //        report.SetParameterValue("@nam", DateTime.Now.Year);
        //        report.SetParameterValue("@ten_cx", query.TEN_CX != null ? query.TEN_CX : "");
        //        report.SetParameterValue("@cmnd", query.SO_CMND != null ? query.SO_CMND : "");
        //        report.SetParameterValue("@ngaysinh",query.NGAY_SINH !=null ? query.NGAY_SINH.Value.ToShortDateString() : "");
        //        report.SetParameterValue("@ngaycap",query.NGAY_CAP != null ? query.NGAY_CAP.Value.ToShortDateString() : "");
        //        report.SetParameterValue("@noicap", query.NOI_CAP != null ? query.NOI_CAP : "");
        //        report.SetParameterValue("@vo", query.VO != null ? query.VO : "");
        //        report.SetParameterValue("@cmnd_vo", query.CMND_VO != null ? query.CMND_VO : "");
        //        report.SetParameterValue("@ngaysinh_vo", query.NGAY_SINH_VO != null ? query.NGAY_SINH_VO.Value.ToShortDateString() : "");
        //        report.SetParameterValue("@ngaycap_vo", query.NGAY_CAP_VO != null ? query.NGAY_CAP_VO.Value.ToShortDateString() : "");
        //        report.SetParameterValue("@noicap_vo", query.NOI_CAP_VO != null ? query.NOI_CAP_VO : "");
        //        report.SetParameterValue("@hktt", query.DK_HKTT != null ? query.DK_HKTT : "");
        //        report.SetParameterValue("@ten_mua", query.TEN_BM != null ? query.TEN_BM : "");
        //        report.SetParameterValue("@cmnd_mua", query.CMND_MUA != null ? query.CMND_MUA : "");
        //        report.SetParameterValue("@ngaysinh_mua", query.NGAY_SINH_MUA != null ? query.NGAY_SINH_MUA.Value.ToShortDateString() : "");
        //        report.SetParameterValue("@ngaycap_mua", query.NGAY_CAP_MUA != null ? query.NGAY_CAP_MUA.Value.ToShortDateString() : "");
        //        report.SetParameterValue("@noicap_mua", query.NOI_CAP_MUA != null ? query.NOI_CAP_MUA : "");
        //        report.SetParameterValue("@hktt_mua", query.HKTT_MUA != null ? query.HKTT_MUA : "");

        //        report.SetParameterValue("@bienso", query.BIEN_SO != null ? query.BIEN_SO : "");
        //        report.SetParameterValue("@mau", query.MAU != null ? query.MAU : "");
        //        report.SetParameterValue("@nhanhieu", query.NHAN_HIEU != null ? query.NHAN_HIEU : "");
        //        report.SetParameterValue("@loaixe", query.LOAI_XE != null ? query.LOAI_XE : "");
        //        report.SetParameterValue("@sokhung", query.SO_KHUNG != null ? query.SO_KHUNG : "");
        //        report.SetParameterValue("@somay", query.SO_MAY != null ? query.SO_MAY : "");

        //        report.SetParameterValue("@so_dk", query.SO_DK != null ? query.SO_DK : "");
        //        report.SetParameterValue("@noicap_dk", query.NOI_CAP_DK != null ? query.NOI_CAP_DK : "");
        //        report.SetParameterValue("@ngay_cap_dk", query.NGAY_CAP_DK != null ? query.NGAY_CAP_DK.Value.ToShortDateString() : "");

        //        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, false, "HOPDONG");

        //        // Clear all sessions value
        //        Session["ReportName"] = null;
        //        Session["ma_xe"] = null;

        //    }
        //    catch (Exception ex)
        //    {
        //        Response.Write(ex.ToString());
        //    }
        //}

        //public AjaxResult getCX()
        //{
        //    AjaxResult result = new AjaxResult();
        //    JsonObject jobject = new JsonObject();
        //    try
        //    {
        //        var mk = (from p in this.DbContext.DM_CHUXEs
        //                  join d in this.DbContext.ACCOUNTs on p.MA_CX equals d.MA_NV
        //                  where d.Username == User.Identity.Name
        //                  select new
        //                  {
        //                      p.MA_CX,
        //                      p.TEN_CX,
        //                      p.GIOI_TINH,
        //                      p.NGAY_SINH,
        //                      p.SO_CMND,
        //                      p.NGAY_CAP,
        //                      p.NOI_CAP,
        //                      p.DK_HKTT,
        //                      p.VO,
        //                      p.NGAY_SINH_VO,
        //                      p.CMND_VO,
        //                      p.NOI_CAP_VO,
        //                      p.NGAY_CAP_VO,
        //                      p.DK_HKTT_VO,
        //                      p.CN_KETHON
        //                  }).First();
        //        jobject.Add("ma", mk.MA_CX);
        //        jobject.Add("ten", mk.TEN_CX);
        //        jobject.Add("gioitinh", mk.GIOI_TINH);
        //        jobject.Add("ngaysinh", mk.NGAY_SINH != null ? mk.NGAY_SINH.Value.ToShortDateString() : "");
        //        jobject.Add("cmnd", mk.SO_CMND);
        //        jobject.Add("ngaycap", mk.NGAY_CAP != null ? mk.NGAY_CAP.Value.ToShortDateString() : "");
        //        jobject.Add("noicap", mk.NOI_CAP);
        //        jobject.Add("hktt", mk.DK_HKTT);
        //        jobject.Add("vo", mk.VO);
        //        jobject.Add("cmnd_vo", mk.CMND_VO);
        //        jobject.Add("ngaysinh_vo", mk.NGAY_SINH_VO != null ? mk.NGAY_SINH_VO.Value.ToShortDateString() : "");
        //        jobject.Add("ngaycap_vo", mk.NGAY_CAP_VO != null ? mk.NGAY_CAP_VO.Value.ToShortDateString() : "");
        //        jobject.Add("noicap_vo", mk.NOI_CAP_VO);
        //        jobject.Add("hktt_vo", mk.DK_HKTT_VO);
        //        jobject.Add("cnkh", mk.CN_KETHON);
        //        result.Result = jobject;
        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        result.ErrorMessage = e.ToString();
        //    }
        //    result.Result = jobject;
        //    return result;
        //}

        //#endregion

        
    }
}
