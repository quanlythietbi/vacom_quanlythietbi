﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using QUANLYBANHANG.Models;
using Ext.Net;
using VACOMLIB;

namespace QUANLYBANHANG.Controllers
{
    public class HethongController : BaseDataController
    {
        //
        // GET: /Hethong/               

        #region "Danh mục cha"

        public ActionResult QuanlyDmCha(){
            return View();
        }

        public AjaxStoreResult dsQlDmCha(int start, int limit)
        {
            var query = (from c in this.DbContext.DANHMUCs                         
                         select new
                         {
                             c.MA_DANHMUC,
                             c.TEN_DANHMUC_VN,
                             c.TEN_DANHMUC_US,
                             c.TEN_DANHMUC_CN,
                             c.TEN_DANHMUC_JP
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }                

        public AjaxFormResult saveDanhmuc(FormCollection values)
        {
            DANHMUC dm;
            AjaxFormResult response = new AjaxFormResult();
            var query = from c in this.DbContext.DANHMUCs
                        select c;
            try
            {
                if (string.IsNullOrEmpty(values["id"]))
                {

                    foreach (DANHMUC p in query)
                    {
                        if (p.MA_DANHMUC.Equals(values["MA_DANHMUC"]))
                        {
                            response.Success = false;
                            response.ExtraParams["msg"] = "Mã danh mục đã tồn tại!";
                            return response;
                        }
                    }

                    dm = new DANHMUC();
                    dm.MA_DANHMUC = values["MA_DANHMUC"];
                    dm.TEN_DANHMUC_VN = values["TEN_DANHMUC_VN"];
                    dm.TEN_DANHMUC_US = values["TEN_DANHMUC_US"];
                    dm.TEN_DANHMUC_CN = values["TEN_DANHMUC_CN"];
                    dm.TEN_DANHMUC_JP = values["TEN_DANHMUC_JP"];
                    dm.TEN_ICON = values["ten_anh"];

                    this.DbContext.DANHMUCs.InsertOnSubmit(dm);
                    this.DbContext.SubmitChanges();
                }
                else
                {
                    if (values["MA_DANHMUC"].Equals(values["id"]))
                    {
                        dm = (from p in this.DbContext.DANHMUCs
                              where p.MA_DANHMUC == values["id"]
                              select p).First();

                        dm.TEN_DANHMUC_VN = values["TEN_DANHMUC_VN"];
                        dm.TEN_DANHMUC_US = values["TEN_DANHMUC_US"];
                        dm.TEN_DANHMUC_CN = values["TEN_DANHMUC_CN"];
                        dm.TEN_DANHMUC_JP = values["TEN_DANHMUC_JP"];
                        if (!string.IsNullOrEmpty(values["ten_anh"]))
                            dm.TEN_ICON = values["ten_anh"];
                        this.DbContext.SubmitChanges();
                    }
                    else
                    {
                        foreach (DANHMUC p in query)
                        {
                            if (p.MA_DANHMUC.Equals(values["MA_DANHMUC"]))
                            {
                                response.Success = false;
                                response.ExtraParams["msg"] = "Mã danh mục đã tồn tại!";
                                return response;
                            }
                        }
                        this.DbContext.ExecuteCommand("UPDATE DANHMUC SET MA_DANHMUC = {0} WHERE MA_DANHMUC = {1}", values["MA_DANHMUC"], values["id"]);
                        dm = (from p in this.DbContext.DANHMUCs
                              where p.MA_DANHMUC == values["MA_DANHMUC"]
                              select p).First();

                        dm.TEN_DANHMUC_VN = values["TEN_DANHMUC_VN"];
                        dm.TEN_DANHMUC_US = values["TEN_DANHMUC_US"];
                        dm.TEN_DANHMUC_CN = values["TEN_DANHMUC_CN"];
                        dm.TEN_DANHMUC_JP = values["TEN_DANHMUC_JP"];
                        if (!string.IsNullOrEmpty(values["ten_anh"]))
                            dm.TEN_ICON = values["ten_anh"];
                        this.DbContext.SubmitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaDanhmuc(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var dv = (from c in this.DbContext.DANHMUCs
                          where c.MA_DANHMUC == id
                          select c).First();
                this.DbContext.DANHMUCs.DeleteOnSubmit(dv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Danh mục con"

        public AjaxStoreResult dsDanhmuc(int start, int limit, string ma)
        {
            if(string.IsNullOrEmpty(ma)){
                var query = (from c in this.DbContext.DANHMUC_CONs
                             select new
                             {
                                 c.MA_DMC,
                                 c.TEN_DMC_VN,
                                 c.TEN_DMC_EN,
                                 c.TEN_DMC_CN,
                                 c.TEN_DMC_JP,
                                 c.LINK_DMC
                             });
                int total = query.ToList().Count;

                query = query.Skip(start).Take(limit);
                return new AjaxStoreResult(query, total);
            }
            else
            {
                var query = (from c in this.DbContext.DANHMUC_CONs
                             select new
                             {
                                 c.MA_DMC,
                                 c.TEN_DMC_VN,
                                 c.TEN_DMC_EN,
                                 c.TEN_DMC_CN,
                                 c.TEN_DMC_JP,
                                 c.LINK_DMC
                             });
                var query1 = (from c in this.DbContext.DANHMUC_CONs
                              join d in this.DbContext.CT_DANHMUCs on c.MA_DMC equals d.MA_DMC
                              join e in this.DbContext.DANHMUCs on d.MA_DANHMUC equals e.MA_DANHMUC
                              where e.MA_DANHMUC == ma
                             select new
                             {
                                 c.MA_DMC,
                                 c.TEN_DMC_VN,
                                 c.TEN_DMC_EN,
                                 c.TEN_DMC_CN,
                                 c.TEN_DMC_JP,
                                 c.LINK_DMC
                             });

                var query2 = query.Except(query1);
                int total = query2.ToList().Count;

                query2 = query2.Skip(start).Take(limit);
                return new AjaxStoreResult(query2, total);
            }
        }

        public AjaxFormResult saveDanhmucCon(FormCollection values)
        {
            DANHMUC_CON dm;
            AjaxFormResult response = new AjaxFormResult();
            var query = from c in this.DbContext.DANHMUC_CONs
                        select c;
            try
            {
                if (string.IsNullOrEmpty(values["id1"]))
                {

                    foreach (DANHMUC_CON p in query)
                    {
                        if (p.MA_DMC.Equals(values["MA_DMC"]))
                        {
                            response.Success = false;
                            response.ExtraParams["msg"] = "Mã danh mục con đã tồn tại!";
                            return response;
                        }
                    }

                    dm = new DANHMUC_CON();
                    dm.MA_DMC = values["MA_DMC"];
                    dm.TEN_DMC_VN = values["TEN_DMC_VN"];
                    dm.TEN_DMC_EN = values["TEN_DMC_EN"];
                    dm.TEN_DMC_CN = values["TEN_DMC_CN"];
                    dm.TEN_DMC_JP = values["TEN_DMC_JP"];
                    dm.LINK_DMC = values["LINK_DMC"];

                    this.DbContext.DANHMUC_CONs.InsertOnSubmit(dm);
                    this.DbContext.SubmitChanges();
                }
                else
                {
                    if (values["MA_DMC"].Equals(values["id1"]))
                    {
                        dm = (from p in this.DbContext.DANHMUC_CONs
                              where p.MA_DMC == values["id1"]
                              select p).First();

                        dm.TEN_DMC_VN = values["TEN_DMC_VN"];
                        dm.TEN_DMC_EN = values["TEN_DMC_EN"];
                        dm.TEN_DMC_CN = values["TEN_DMC_CN"];
                        dm.TEN_DMC_JP = values["TEN_DMC_JP"];
                        dm.LINK_DMC = values["LINK_DMC"];

                        this.DbContext.SubmitChanges();
                    }
                    else
                    {
                        foreach (DANHMUC_CON p in query)
                        {
                            if (p.MA_DMC.Equals(values["MA_DMC"]))
                            {
                                response.Success = false;
                                response.ExtraParams["msg"] = "Mã danh mục con đã tồn tại!";
                                return response;
                            }
                        }
                        this.DbContext.ExecuteCommand("UPDATE DANHMUC_CON SET MA_DMC = {0} WHERE MA_DMC = {1}", values["MA_DMC"], values["id1"]);
                        this.DbContext.ExecuteCommand("UPDATE DANHMUC_CON SET TEN_DMC_VN = {0}, TEN_DMC_EN = {1},TEN_DMC_CN = {2},TEN_DMC_JP = {3}, LINK_DMC = {4} WHERE MA_DMC = {5}",
                            values["TEN_DMC_VN"], values["TEN_DMC_EN"], values["TEN_DMC_CN"], values["TEN_DMC_JP"], values["MA_DMC"]);
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaDanhmucCon(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var dm = (from c in this.DbContext.DANHMUC_CONs
                          where c.MA_DMC == id
                          select c).First();
                this.DbContext.DANHMUC_CONs.DeleteOnSubmit(dm);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Chi tiết danh mục"

        public ActionResult QuanlyDanhmuc()
        {
            return View();
        }

        public ActionResult ctQuanlyTree()
        {
            return View();
        }

        public AjaxStoreResult dsQlDanhmuc(int start, int limit)
        {
            var query = (from c in this.DbContext.DANHMUCs
                         join d in this.DbContext.CT_DANHMUCs on c.MA_DANHMUC equals d.MA_DANHMUC
                         join e in this.DbContext.DANHMUC_CONs on d.MA_DMC equals e.MA_DMC
                         select new
                         {
                             d.MA_CTDM,
                             c.MA_DANHMUC,
                             c.TEN_DANHMUC_VN,
                             e.MA_DMC,
                             e.TEN_DMC_VN,
                             d.KIEU_DM_VN
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxStoreResult dsKieuDm(int start, int limit, string dm)
        {
            var query = (from c in this.DbContext.CT_DANHMUCs
                         where c.MA_DANHMUC == dm
                         select new
                         {
                             c.KIEU_DM_VN
                         }).Distinct();
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxResult getCheck(string madm, string kieu, string kieukhac_vn,string kieukhac_en,string kieukhac_cn,string kieukhac_jp)
        {
            CT_DANHMUC ct;
            AjaxResult response = new AjaxResult();
            try
            {
                //var qry = this.DbContext.sp_xoaKT(ma_nv, kl);
                List<string> s = (List<string>)System.Web.HttpContext.Current.Session["check"];
                foreach (string id in s)
                {
                    ct = new CT_DANHMUC();
                    ct.MA_DANHMUC = madm;
                    ct.MA_DMC = id;
                        if (string.IsNullOrEmpty(kieukhac_vn))
                        {
                            var dm = (from p in this.DbContext.CT_DANHMUCs
                                     where p.MA_DANHMUC == madm && p.KIEU_DM_VN == kieu
                                     select  new {
                                        p.KIEU_DM_VN,
                                        p.KIEU_DM_EN,
                                        p.KIEU_DM_CN,
                                        p.KIEU_DM_JP
                                     }).First();
                            ct.KIEU_DM_VN = kieu;
                            ct.KIEU_DM_EN = dm.KIEU_DM_EN;
                            ct.KIEU_DM_CN = dm.KIEU_DM_CN;
                            ct.KIEU_DM_JP = dm.KIEU_DM_JP;
                        }
                        else
                        {
                            ct.KIEU_DM_VN = kieukhac_vn;
                            ct.KIEU_DM_EN = kieukhac_en;
                            ct.KIEU_DM_CN = kieukhac_cn;
                            ct.KIEU_DM_JP = kieukhac_jp;
                        }
                    this.DbContext.CT_DANHMUCs.InsertOnSubmit(ct);
                    this.DbContext.SubmitChanges();
                }
                Session["check"] = null;
                return response;
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
                return response;
            }
        }

        [HttpPost]
        public AjaxResult xoaCT(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var dm = (from p in this.DbContext.CT_DANHMUCs
                          where p.MA_CTDM == Convert.ToInt32(id)
                          select p).First();
                this.DbContext.CT_DANHMUCs.DeleteOnSubmit(dm);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Nhân viên"

        public ActionResult QlNhanvien()
        {
            return View();
        }

        public AjaxStoreResult dsNhanvien(int start, int limit)
        {
            var query = (from c in this.DbContext.NHANVIENs
                         orderby c.PHONG ascending
                         select new
                         {
                             c.MA_NV,
                             c.TEN_NV,
                             c.PHONG,
                             c.CHUCVU,
                             c.CMND,
                             c.DIACHI,
                             c.EMAIL,
                             c.GIOI_TINH,
                             c.HOKHAU,
                             c.NGAY_CAP,
                             c.NGAY_SINH,
                             c.NOI_CAP,
                             c.SDD,
                             c.SDT,
                             c.SDT_FAMILY,
                             c.TAIKHOAN,
                             c.NICK_CHAT,
                             c.TEN_TAT,
                             c.GHICHU
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxStoreResult dsNhanvien_Luong(int start, int limit)
        {
            var query = (from c in this.DbContext.NHANVIENs
                         where c.TRANGTHAI != true
                         select new
                         {
                             c.MA_NV,
                             c.TEN_NV,
                             c.PHONG,
                             c.CHUCVU,
                             c.CMND,
                             c.DIACHI,
                             c.EMAIL,
                             c.GIOI_TINH,
                             c.HOKHAU,
                             c.NGAY_CAP,
                             c.NGAY_SINH,
                             c.NOI_CAP,
                             c.SDD,
                             c.SDT,
                             c.SDT_FAMILY,
                             c.TAIKHOAN,
                             c.NICK_CHAT,
                             c.TEN_TAT,
                             c.GHICHU
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveNhanvien(FormCollection values)
        {
            NHANVIEN dm;
            AjaxFormResult response = new AjaxFormResult();
            var query = from c in this.DbContext.NHANVIENs
                        select c;
            try
            {
                if (string.IsNullOrEmpty(values["id"]))
                {

                    foreach (NHANVIEN p in query)
                    {
                        if (p.MA_NV.Equals(values["MA_NV"]))
                        {
                            response.Success = false;
                            response.ExtraParams["msg"] = "Mã nhân viên đã tồn tại!";
                            return response;
                        }
                    }

                    dm = new NHANVIEN();
                    dm.MA_NV = values["MA_NV"];
                    dm.TEN_NV = values["TEN_NV"];
                    dm.PHONG = values["PHONG_Value"];
                    dm.CHUCVU = values["CHUCVU_Value"];
                    if (!string.IsNullOrEmpty(values["NGAY_SINH"]))
                        dm.NGAY_SINH = Convert.ToDateTime(values["NGAY_SINH"]);
                    if (values["grGioitinh_Group"] == "nam")
                        dm.GIOI_TINH = true;
                    else
                        dm.GIOI_TINH = false;
                    dm.EMAIL = values["EMAIL"];
                    dm.DIACHI = values["DIACHI"];                    
                    dm.TAIKHOAN = values["TAIKHOAN"];
                    dm.SDT = values["SDT"];
                    dm.SDD = values["SDD"];
                    dm.SDT_FAMILY = values["SDT_FAMILY"];
                    dm.TEN_TAT = values["TEN_TAT"];
                   
                    dm.GHICHU = values["GHICHU"];

                    this.DbContext.NHANVIENs.InsertOnSubmit(dm);
                    this.DbContext.SubmitChanges();
                }
                else
                {
                    if (values["MA_NV"].Equals(values["id"]))
                    {
                        dm = (from p in this.DbContext.NHANVIENs
                              where p.MA_NV == values["id"]
                              select p).First();

                        dm.TEN_NV = values["TEN_NV"];
                        dm.PHONG = values["PHONG_Value"];
                        dm.CHUCVU = values["CHUCVU_Value"];
                        if (!string.IsNullOrEmpty(values["NGAY_SINH"]))
                            dm.NGAY_SINH = Convert.ToDateTime(values["NGAY_SINH"]);
                        if (values["grGioitinh_Group"] == "rdNam")
                            dm.GIOI_TINH = true;
                        else
                            dm.GIOI_TINH = false;
                      
                        dm.EMAIL = values["EMAIL"];
                        dm.DIACHI = values["DIACHI"];
                        dm.TAIKHOAN = values["TAIKHOAN"];
                        dm.SDT = values["SDT"];
                        dm.SDD = values["SDD"];
                        dm.SDT_FAMILY = values["SDT_FAMILY"];
                        dm.TEN_TAT = values["TEN_TAT"];
                        dm.GHICHU = values["GHICHU"];
                        this.DbContext.SubmitChanges();
                    }
                    else
                    {
                        foreach (NHANVIEN p in query)
                        {
                            if (p.MA_NV.Equals(values["MA_NV"]))
                            {
                                response.Success = false;
                                response.ExtraParams["msg"] = "Mã nhân viên đã tồn tại!";
                                return response;
                            }
                        }
                        this.DbContext.ExecuteCommand("UPDATE NHANVIEN SET MA_NV = {0} WHERE MA_NV = {1}", values["MA_NV"], values["id"]);
                        dm = (from p in this.DbContext.NHANVIENs
                              where p.MA_NV == values["MA_NV"]
                              select p).First();

                        dm.TEN_NV = values["TEN_NV"];
                        dm.PHONG = values["PHONG_Value"];
                        dm.CHUCVU = values["CHUCVU_Value"];
                        if (!string.IsNullOrEmpty(values["NGAY_SINH"]))
                            dm.NGAY_SINH = Convert.ToDateTime(values["NGAY_SINH"]);
                        if (values["grGioitinh_Group"] == "rdNam")
                            dm.GIOI_TINH = true;
                        else
                            dm.GIOI_TINH = false;
                        dm.EMAIL = values["EMAIL"];
                        dm.DIACHI = values["DIACHI"];
                        dm.TAIKHOAN = values["TAIKHOAN"];
                        dm.SDT = values["SDT"];
                        dm.SDD = values["SDD"];
                        dm.SDT_FAMILY = values["SDT_FAMILY"];
                        dm.TEN_TAT = values["TEN_TAT"];
                        dm.GHICHU = values["GHICHU"];
                        this.DbContext.SubmitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaNhanvien(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.NHANVIENs
                          where c.MA_NV == id
                          select c).First();
                this.DbContext.NHANVIENs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        [HttpPost]
        public AjaxResult doiMK(string cu, string moi, string moi1)
        {
            AjaxResult result = new AjaxResult();
            JsonObject jobject = new JsonObject();
            try
            {
                var mk = (from p in this.DbContext.ACCOUNTs
                          where p.Username == User.Identity.Name
                          select new
                          {
                              p.Pass
                          }).First();
                Encrypt en = new Encrypt(mk.Pass);
                if (en.CheckPassword(cu) && moi.Equals(moi1))
                {
                    var mk1 = (from p in this.DbContext.ACCOUNTs
                               where p.Username == User.Identity.Name
                               select p).First();
                    mk1.Pass = en.CreateHashedPassword(moi, null);
                    this.DbContext.SubmitChanges();
                    jobject.Add("sai", "");
                    result.Result = jobject;
                    return result;
                }
                else
                {
                    jobject.Add("sai", "Kiểm tra thông tin nhập!");
                    result.Result = jobject;
                    return result;
                }
            }
            catch (Exception e)
            {
                result.ErrorMessage = e.ToString();
            }
            result.Result = jobject;
            return result;
        }

        #endregion

        #region "Phân quyền"

        public ActionResult QTHethong()
        {
            return View();
        }

        public AjaxStoreResult dsUser(int start, int limit)
        {
            var query = (from c in this.DbContext.ACCOUNTs
                         join d in this.DbContext.NHOM_USERs on c.MA_NHOM equals d.MA_NHOM into ps
                         from d in ps.DefaultIfEmpty()
                         join b in this.DbContext.NHANVIENs on c.MA_NV equals b.MA_NV into ps_ns
                         from b in ps_ns.DefaultIfEmpty()
                         select new
                         {
                             c.MA_NV,
                             b.TEN_NV,
                             c.Username,
                             c.Language,
                             c.Theme,
                             ROLE = d.TEN_NHOM,
                             c.Pass,
                             c.GHICHU
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveUser(FormCollection values)
        {
            ACCOUNT dm;
            AjaxFormResult response = new AjaxFormResult();
            var query = from c in this.DbContext.ACCOUNTs
                        select c;
            Encrypt crypt = new Encrypt();
            try
            {
                if (string.IsNullOrEmpty(values["id"]))
                {

                    foreach (ACCOUNT p in query)
                    {
                        if (p.Username.Equals(values["Username"]))
                        {
                            response.Success = false;
                            response.ExtraParams["msg"] = "Tên truy cập đã tồn tại!";
                            return response;
                        }
                    }

                    dm = new ACCOUNT();
                    dm.Username = values["Username"];
                    dm.Pass = crypt.CreateHashedPassword(values["Pass1"], null);
                    dm.MA_NV = values["NHANVIEN_Value"];
                    dm.MA_NHOM = Convert.ToInt32(values["MA_NHOM_Value"]);
                    dm.GHICHU = values["GHICHU"];

                    this.DbContext.ACCOUNTs.InsertOnSubmit(dm);
                    this.DbContext.SubmitChanges();
                }
                else
                {
                    if (values["Username"].Equals(values["id"]))
                    {
                        dm = (from p in this.DbContext.ACCOUNTs
                              where p.Username == values["id"]
                              select p).First();
                        if (!string.IsNullOrEmpty(values["Pass1"]))
                            dm.Pass = crypt.CreateHashedPassword(values["Pass1"], null);
                        dm.MA_NV = values["NHANVIEN_Value"];
                        dm.MA_NHOM = Convert.ToInt32(values["MA_NHOM_Value"]);
                        dm.GHICHU = values["GHICHU"];
                        this.DbContext.SubmitChanges();
                    }
                    else
                    {
                        foreach (ACCOUNT p in query)
                        {
                            if (p.Username.Equals(values["Username"]))
                            {
                                response.Success = false;
                                response.ExtraParams["msg"] = "Tên truy cập đã tồn tại!";
                                return response;
                            }
                        }
                        this.DbContext.ExecuteCommand("UPDATE ACCOUNT SET Username = {0} WHERE Username = {1}", values["Username"], values["id"]);
                        dm = (from p in this.DbContext.ACCOUNTs
                              where p.Username == values["Username"]
                              select p).First();
                        if (!string.IsNullOrEmpty(values["Pass1"]))
                            dm.Pass = crypt.CreateHashedPassword(values["Pass1"], null);
                        dm.MA_NV = values["NHANVIEN_Value"];
                        dm.MA_NHOM = Convert.ToInt32(values["MA_NHOM_Value"]);
                        dm.GHICHU = values["GHICHU"];
                        this.DbContext.SubmitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaUser(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.ACCOUNTs
                          where c.Username == id
                          select c).First();
                this.DbContext.ACCOUNTs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        //public AjaxStoreResult dsQuyen(string TenTruyCap)
        //{
        //    ACCOUNT user = (from p in this.DbContext.ACCOUNTs
        //                    where p.Username == User.Identity.Name
        //                    select p).First();


        //    ACCOUNT grp = (from c in this.DbContext.ACCOUNTs
        //                   where c.Username == TenTruyCap
        //                   select c).Single<ACCOUNT>();
        //    var query1 = (from c in this.DbContext.PhanQuyens
        //                  join a in this.DbContext.DANHMUC_CONs on c.MA_DMC equals a.MA_DMC
        //                  join e in this.DbContext.CT_DANHMUCs on a.MA_DMC equals e.MA_DMC
        //                  join b in this.DbContext.ACCOUNTs on c.Username equals b.Username
        //                  join d in this.DbContext.DANHMUCs on e.MA_DANHMUC equals d.MA_DANHMUC
        //                  where b.Username == TenTruyCap
        //                  select new
        //                  {
        //                      Check = true,
        //                      NEW = Convert.ToString(c.Them) == "true" ? true : false,
        //                      EDIT = Convert.ToString(c.Sua) == "true" ? true : false,
        //                      DEL = Convert.ToString(c.Xoa) == "true" ? true : false,
        //                      USER = Convert.ToString(c.User) == "true" ? true : false,
        //                      a.MA_DMC,
        //                      a.TEN_DMC_VN,
        //                      a.LINK_DMC,
        //                      d.MA_DANHMUC,
        //                      d.TEN_DANHMUC_VN
        //                  }).Distinct();

        //    var query2 = (from o in this.DbContext.DANHMUCs
        //                  join d in this.DbContext.CT_DANHMUCs on o.MA_DANHMUC equals d.MA_DANHMUC
        //                  join e in this.DbContext.DANHMUC_CONs on d.MA_DMC equals e.MA_DMC
        //                  join g in this.DbContext.PhanQuyens on e.MA_DMC equals g.MA_DMC
        //                  where (!(from f in this.DbContext.PhanQuyens
        //                           where f.Username == TenTruyCap
        //                           select f.MA_DMC).Contains(e.MA_DMC))
        //                  select new
        //                  {
        //                      Check = false,
        //                      NEW = false,
        //                      EDIT = false,
        //                      DEL = false,
        //                      USER = false,
        //                      e.MA_DMC,
        //                      e.TEN_DMC_VN,
        //                      e.LINK_DMC,
        //                      o.MA_DANHMUC,
        //                      o.TEN_DANHMUC_VN
        //                  });

        //    var query = query1.Union(query2);
        //    return new AjaxStoreResult(query);
        //}

        //[HttpPost]
        //public AjaxResult AddUserItems(string TenTruyCap, string json)
        //{
        //    AjaxResult response = new AjaxResult();
        //    Dictionary<string, string>[] companies = JSON.Deserialize<Dictionary<string, string>[]>(json);

        //    try
        //    {
        //        //lay danh sach cac danh muc cua UserName
        //        var listUserItem = (from c in this.DbContext.PhanQuyens where c.Username == TenTruyCap select c);

        //        //Xoa toan bo danh sach cua UserName
        //        foreach (PhanQuyen item in listUserItem)
        //        {
        //            this.DbContext.PhanQuyens.DeleteOnSubmit(item);
        //        }

        //        //Luu cac danh muc moi
        //        if (!string.IsNullOrEmpty(json))
        //        {
        //            foreach (Dictionary<string, string> row in companies)
        //            {
        //                PhanQuyen grpItem = new PhanQuyen();
        //                grpItem.Username = TenTruyCap;
        //                grpItem.MA_DMC = row["MA_DMC"];
        //                grpItem.Xoa = Convert.ToBoolean(row["DEL"]);
        //                grpItem.Them = Convert.ToBoolean(row["NEW"]);
        //                grpItem.Sua = Convert.ToBoolean(row["EDIT"]);
        //                grpItem.User = Convert.ToBoolean(row["USER"]);
        //                this.DbContext.PhanQuyens.InsertOnSubmit(grpItem);
        //            }
        //        }
        //        this.DbContext.SubmitChanges();
        //    }
        //    catch (Exception e)
        //    {
        //        response.ErrorMessage = e.ToString();
        //    }
        //    return response;
        //}

        #endregion

        #region "Phân quyền theo user"

        public ActionResult Dm_NhomUser()
        {
            return View();
        }

        public AjaxStoreResult dsNhomUser(int start, int limit)
        {
            var query = (from c in this.DbContext.NHOM_USERs
                         select new
                         {
                             c.MA_NHOM,
                             c.TEN_NHOM,
                             c.GHICHU
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveNhomUser(FormCollection values)
        {
            NHOM_USER pyc;
            AjaxFormResult response = new AjaxFormResult();
            var query = from c in this.DbContext.NHOM_USERs
                        select c;
            var nv = from p in this.DbContext.ACCOUNTs
                     where p.Username == User.Identity.Name
                     select p;
            try
            {
                if (string.IsNullOrEmpty(values["id"]))
                {
                    pyc = new NHOM_USER();
                    pyc.TEN_NHOM = values["TEN_NHOM"];
                    pyc.GHICHU = values["GHICHU"];
                    this.DbContext.NHOM_USERs.InsertOnSubmit(pyc);
                    this.DbContext.SubmitChanges();
                }
                else
                {
                    pyc = (from p in this.DbContext.NHOM_USERs
                           where p.MA_NHOM == Convert.ToInt32(values["id"])
                           select p).First();

                    pyc.TEN_NHOM = values["TEN_NHOM"];
                    pyc.GHICHU = values["GHICHU"];

                    this.DbContext.SubmitChanges();
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaNhomUser(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.NHOM_USERs
                          where c.MA_NHOM == Convert.ToInt32(id)
                          select c).First();
                this.DbContext.NHOM_USERs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        public AjaxStoreResult dsCTNhomUser(int start, int limit, string ma_nhom)
        {
            var query = (from c in this.DbContext.CT_QUYEN_NHOM_USERs
                         join d in this.DbContext.NHOM_USERs on c.MA_NHOM equals d.MA_NHOM
                         join e in this.DbContext.DANHMUC_CONs on c.MA_DMC equals e.MA_DMC
                         where c.MA_NHOM == Convert.ToInt32(ma_nhom)
                         select new
                         {
                             c.MA_CT,
                             c.MA_NHOM,
                             c.MA_DMC,
                             TEN_DMC = e.TEN_DMC_VN,
                             d.TEN_NHOM,
                             THEM = c.Them,
                             SUA = c.Sua,
                             XOA = c.Xoa,
                             USER = c.User
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxStoreResult dsCTQuyen(string ma_nhom)
        {

            var query1 = (from c in this.DbContext.CT_QUYEN_NHOM_USERs
                          join a in this.DbContext.DANHMUC_CONs on c.MA_DMC equals a.MA_DMC
                          join e in this.DbContext.CT_DANHMUCs on a.MA_DMC equals e.MA_DMC
                          join b in this.DbContext.NHOM_USERs on c.MA_NHOM equals b.MA_NHOM
                          join d in this.DbContext.DANHMUCs on e.MA_DANHMUC equals d.MA_DANHMUC
                          where c.MA_NHOM == Convert.ToInt32(ma_nhom)
                          select new
                          {
                              Check = true,
                              NEW = Convert.ToString(c.Them) == "true" ? true : false,
                              EDIT = Convert.ToString(c.Sua) == "true" ? true : false,
                              DEL = Convert.ToString(c.Xoa) == "true" ? true : false,
                              USER = Convert.ToString(c.User) == "true" ? true : false,
                              a.MA_DMC,
                              a.TEN_DMC_VN,
                              a.LINK_DMC,
                              d.MA_DANHMUC,
                              d.TEN_DANHMUC_VN
                          }).Distinct();

            var query2 = (from o in this.DbContext.DANHMUCs
                          join d in this.DbContext.CT_DANHMUCs on o.MA_DANHMUC equals d.MA_DANHMUC
                          join e in this.DbContext.DANHMUC_CONs on d.MA_DMC equals e.MA_DMC
                          select new
                          {
                              Check = false,
                              NEW = false,
                              EDIT = false,
                              DEL = false,
                              USER = false,
                              e.MA_DMC,
                              e.TEN_DMC_VN,
                              e.LINK_DMC,
                              o.MA_DANHMUC,
                              o.TEN_DANHMUC_VN
                          }).Except(query1);

            var query = query1.Union(query2);
            return new AjaxStoreResult(query);
        }

        [HttpPost]
        public AjaxResult AddQuyenUser(string ma_nhom, string json)
        {
            AjaxResult response = new AjaxResult();
            Dictionary<string, string>[] companies = JSON.Deserialize<Dictionary<string, string>[]>(json);

            try
            {
                //lay danh sach cac danh muc cua UserName
                var listUserItem = (from c in this.DbContext.CT_QUYEN_NHOM_USERs where c.MA_NHOM == Convert.ToInt32(ma_nhom) select c);

                //Xoa toan bo danh sach cua UserName
                foreach (CT_QUYEN_NHOM_USER item in listUserItem)
                {
                    this.DbContext.CT_QUYEN_NHOM_USERs.DeleteOnSubmit(item);
                }

                //Luu cac danh muc moi
                if (!string.IsNullOrEmpty(json))
                {
                    foreach (Dictionary<string, string> row in companies)
                    {
                        CT_QUYEN_NHOM_USER grpItem = new CT_QUYEN_NHOM_USER();
                        grpItem.MA_NHOM = Convert.ToInt32(ma_nhom);
                        grpItem.MA_DMC = row["MA_DMC"];
                        grpItem.Xoa = Convert.ToBoolean(row["DEL"]);
                        grpItem.Them = Convert.ToBoolean(row["NEW"]);
                        grpItem.Sua = Convert.ToBoolean(row["EDIT"]);
                        grpItem.User = Convert.ToBoolean(row["USER"]);
                        this.DbContext.CT_QUYEN_NHOM_USERs.InsertOnSubmit(grpItem);
                    }
                }
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }
               

        #endregion
    }
}
